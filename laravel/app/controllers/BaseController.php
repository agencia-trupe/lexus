<?php

use \FaleConosco, \SeguroPessoal, \SeguroEmpresarial;

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout)->with('infoContato', FaleConosco::first())
													 ->with('listaSegurosPessoais', SeguroPessoal::ordenado())
													 ->with('listaSegurosEmpresas', SeguroEmpresarial::ordenado());
		}
	}

}
