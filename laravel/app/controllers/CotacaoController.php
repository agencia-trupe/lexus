<?php

use \CotacoesOutras, \CotacoesAuto, \SeguroEmpresarial, \SeguroPessoal;

class CotacaoController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function auto()
	{
		$this->layout->with('templateCSS', array('css/cotacao'));
		$this->layout->content = view::make('frontend.cotacao.auto');
	}

	public function gravarAuto()
	{
		$data['nome'] = Input::get('nome');
		$data['telefone'] = Input::get('telefone');
		$data['email'] = Input::get('email');
		$data['renovacao_seguradora'] = Input::get('renovacao_seguradora');
		$data['renovacao_bonus'] = Input::get('renovacao_bonus');
		$data['renovacao_apolice'] = Input::get('renovacao_apolice');
		$data['renovacao_sinistro'] = Input::get('renovacao_sinistro');
		$data['renovacao_vigencia'] = Input::get('renovacao_vigencia');
		$data['segurado_nome'] = Input::get('segurado_nome');
		$data['segurado_cpfcnpj'] = Input::get('segurado_cpfcnpj');
		$data['segurado_data_nascimento'] = Input::get('segurado_data_nascimento');
		$data['segurado_estado_civil'] = Input::get('segurado_estado_civil');
		$data['segurado_residencia_cep'] = Input::get('segurado_residencia_cep');
		$data['segurado_residencia_endereco'] = Input::get('segurado_residencia_endereco');
		$data['segurado_pernoite_cep'] = Input::get('segurado_pernoite_cep');
		$data['segurado_pernoite_endereco'] = Input::get('segurado_pernoite_endereco');
		$data['veiculo_modelo'] = Input::get('veiculo_modelo');
		$data['veiculo_ano_fabricacao'] = Input::get('veiculo_ano_fabricacao');
		$data['veiculo_ano_modelo'] = Input::get('veiculo_ano_modelo');
		$data['veiculo_combustivel'] = Input::get('veiculo_combustivel');
		$data['veiculo_placa'] = Input::get('veiculo_placa');
		$data['veiculo_chassis'] = Input::get('veiculo_chassis');
		$data['veiculo_proprietario'] = Input::get('veiculo_proprietario');
		$data['condutor_nome'] = Input::get('condutor_nome');
		$data['condutor_cpf'] = Input::get('condutor_cpf');
		$data['condutor_relacao'] = Input::get('condutor_relacao');
		$data['condutor_data_nascimento'] = Input::get('condutor_data_nascimento');
		$data['condutor_sexo'] = Input::get('condutor_sexo');
		$data['condutor_estado_civil'] = Input::get('condutor_estado_civil');
		$data['condutor_num_carros'] = Input::get('condutor_num_carros');
		$data['garagem_residencia'] = Input::get('garagem_residencia');
		$data['garagem_residencia_tipo'] = Input::get('garagem_residencia_tipo');
		$data['garagem_trabalho'] = Input::get('garagem_trabalho');
		$data['garagem_trabalho_tipo'] = Input::get('garagem_trabalho_tipo');
		$data['garagem_escola'] = Input::get('garagem_escola');
		$data['garagem_escola_tipo'] = Input::get('garagem_escola_tipo');
		$data['cobertura_condutores_risco'] = Input::get('cobertura_condutores_risco');
		$data['observacoes'] = Input::get('observacoes');

		$validator = \Validator::make(
			array(
		        'nome' => $data['nome'],
				'telefone' => $data['telefone'],
				'email' => $data['email'],
				'segurado_nome' => $data['segurado_nome'],
				'segurado_cpfcnpj' => $data['segurado_cpfcnpj'],
				'segurado_data_nascimento' => $data['segurado_data_nascimento'],
				'segurado_estado_civil' => $data['segurado_estado_civil'],
				'segurado_residencia_cep' => $data['segurado_residencia_cep'],
				'segurado_residencia_endereco' => $data['segurado_residencia_endereco'],
				'segurado_pernoite_cep' => $data['segurado_pernoite_cep'],
				'segurado_pernoite_endereco' => $data['segurado_pernoite_endereco'],
				'veiculo_modelo' => $data['veiculo_modelo'],
				'veiculo_ano_fabricacao' => $data['veiculo_ano_fabricacao'],
				'veiculo_ano_modelo' => $data['veiculo_ano_modelo'],
				'veiculo_combustivel' => $data['veiculo_combustivel'],
				'veiculo_placa' => $data['veiculo_placa'],
				'veiculo_chassis' => $data['veiculo_chassis'],
				'veiculo_proprietario' => $data['veiculo_proprietario'],
				'condutor_nome' => $data['condutor_nome'],
				'condutor_cpf' => $data['condutor_cpf'],
				'condutor_relacao' => $data['condutor_relacao'],
				'condutor_data_nascimento' => $data['condutor_data_nascimento'],
				'condutor_sexo' => $data['condutor_sexo'],
				'condutor_estado_civil' => $data['condutor_estado_civil'],
				'condutor_num_carros' => $data['condutor_num_carros'],
				'garagem_residencia' => $data['garagem_residencia'],
				'garagem_residencia_tipo' => $data['garagem_residencia_tipo'],
				'garagem_trabalho' => $data['garagem_trabalho'],
				'garagem_trabalho_tipo' => $data['garagem_trabalho_tipo'],
				'garagem_escola' => $data['garagem_escola'],
				'garagem_escola_tipo' => $data['garagem_escola_tipo'],
				'cobertura_condutores_risco' => $data['cobertura_condutores_risco']
		    ),
		    array(
		        'nome' => 'required',
				'telefone' => 'required',
				'email' => 'required|email',
				'segurado_nome' => 'required',
				'segurado_cpfcnpj' => 'required',
				'segurado_data_nascimento' => 'required|date_format:d/m/Y',
				'segurado_estado_civil' => 'required',
				'segurado_residencia_cep' => 'required',
				'segurado_residencia_endereco' => 'required',
				'segurado_pernoite_cep' => 'required',
				'segurado_pernoite_endereco' => 'required',
				'veiculo_modelo' => 'required',
				'veiculo_ano_fabricacao' => 'required',
				'veiculo_ano_modelo' => 'required',
				'veiculo_combustivel' => 'required',
				'veiculo_placa' => 'required',
				'veiculo_chassis' => 'required',
				'veiculo_proprietario' => 'required',
				'condutor_nome' => 'required',
				'condutor_cpf' => 'required',
				'condutor_relacao' => 'required',
				'condutor_data_nascimento' => 'required|date_format:d/m/Y',
				'condutor_sexo' => 'required',
				'condutor_estado_civil' => 'required',
				'condutor_num_carros' => 'required',
				'garagem_residencia' => 'required',
				'garagem_residencia_tipo' => 'required',
				'garagem_trabalho' => 'required',
				'garagem_trabalho_tipo' => 'required',
				'garagem_escola' => 'required',
				'garagem_escola_tipo' => 'required',
				'cobertura_condutores_risco' => 'required'
		    ),
			array(
				'nome.required' => "Informe seu nome!",
				'telefone.required' => "Informe seu telefone!",
				'email.required' => "Informe seu E-mail!",
				'email.email' => "Informe um E-mail válido!",
				'segurado_nome.required' => "Informe o nome do Segurado!",
				'segurado_cpfcnpj.required' => "Informe o CPF ou CNPJ do Segurado!",
				'segurado_data_nascimento.required' => "Informe a data de nascimento do Segurado!",
				'segurado_data_nascimento.date_format' => "Informe uma Data de Nascimento do Segurado válida!",
				'segurado_estado_civil.required' => "Informe o estado civil do Segurado!",
				'segurado_residencia_cep.required' => "Informe o cep do Segurado!",
				'segurado_residencia_endereco.required' => "Informe o endereço do segurado!",
				'segurado_pernoite_cep.required' => "Informe o CEP do local de Pernoite!",
				'segurado_pernoite_endereco.required' => "Informe o Endereço do local de Pernoite!",
				'veiculo_modelo.required' => "Informe o modelo do veículo!",
				'veiculo_ano_fabricacao.required' => "Informe o ano de fabricação do veículo!",
				'veiculo_ano_modelo.required' => "Informe o ano modelo do veículo!",
				'veiculo_combustivel.required' => "Informe o combustível do veículo!",
				'veiculo_placa.required' => "Informe a placa do veículo!",
				'veiculo_chassis.required' => "Informe o chassis do veículo!",
				'veiculo_proprietario.required' => "Informe o nome do proprietário do veículo!",
				'condutor_nome.required' => "Informe o nome do condutor principal!",
				'condutor_cpf.required' => "Informe o cpf do condutor principal!",
				'condutor_relacao.required' => "Informe a relação do condutor principal!",
				'condutor_data_nascimento.required' => "Informe a data de nascimento do condutor principal!",
				'condutor_data_nascimento.date_format' => "Informe uma Data de Nascimento do Condutor válida!",
				'condutor_sexo.required' => "Informe o sexo do condutor principal!",
				'condutor_estado_civil.required' => "Informe o estado civil do condutor principal!",
				'condutor_num_carros.required' => "Informe o número de carros da residência!",
				'garagem_residencia.required' => "Informe se o veículo ficará guardado em garagem na residência!",
				'garagem_residencia_tipo.required' => "Informe se a garagem de residência possui portão automático ou porteiro!",
				'garagem_trabalho.required' => "Informe se o veículo ficará guardado em garagem no trabalho!",
				'garagem_trabalho_tipo.required' => "Informe se a garagem do trabalho possui portão automático ou porteiro!",
				'garagem_escola.required' => "Informe se o veículo ficará guardado em garagem na escola/faculda!de",
				'garagem_escola_tipo.required' => "Informe se a garagem da escola/faculdade possui portão automático ou porteiro!",
				'cobertura_condutores_risco.required' => "Informe se deseja cobertura para condutores de 18 a 25 anos!"
			)
		);

		if ($validator->fails())
		{
			foreach ($validator->messages()->all() as $k => $message)
			{
			    if($k == 0)
			    {
				 	Session::flash('erroEnvioCotacao', $message);
				 	break;
			    }
			}
		 	Session::flash('formularioAuto', $data);
			return Redirect::to('cotacao/auto');
		}

		$cotacao = new CotacoesAuto;
		$cotacao->nome = $data['nome'];
		$cotacao->telefone = $data['telefone'];
		$cotacao->email = $data['email'];
		$cotacao->renovacao_seguradora = $data['renovacao_seguradora'];
		$cotacao->renovacao_bonus = $data['renovacao_bonus'];
		$cotacao->renovacao_apolice = $data['renovacao_apolice'];
		$cotacao->renovacao_sinistro = $data['renovacao_sinistro'];
		$cotacao->renovacao_vigencia = $data['renovacao_vigencia'];
		$cotacao->segurado_nome = $data['segurado_nome'];
		$cotacao->segurado_cpfcnpj = $data['segurado_cpfcnpj'];
		$cotacao->segurado_data_nascimento = Tools::converteData($data['segurado_data_nascimento']);
		$cotacao->segurado_estado_civil = $data['segurado_estado_civil'];
		$cotacao->segurado_residencia_cep = $data['segurado_residencia_cep'];
		$cotacao->segurado_residencia_endereco = $data['segurado_residencia_endereco'];
		$cotacao->segurado_pernoite_cep = $data['segurado_pernoite_cep'];
		$cotacao->segurado_pernoite_endereco = $data['segurado_pernoite_endereco'];
		$cotacao->veiculo_modelo = $data['veiculo_modelo'];
		$cotacao->veiculo_ano_fabricacao = $data['veiculo_ano_fabricacao'];
		$cotacao->veiculo_ano_modelo = $data['veiculo_ano_modelo'];
		$cotacao->veiculo_combustivel = $data['veiculo_combustivel'];
		$cotacao->veiculo_placa = $data['veiculo_placa'];
		$cotacao->veiculo_chassis = $data['veiculo_chassis'];
		$cotacao->veiculo_proprietario = $data['veiculo_proprietario'];
		$cotacao->condutor_nome = $data['condutor_nome'];
		$cotacao->condutor_cpf = $data['condutor_cpf'];
		$cotacao->condutor_relacao = $data['condutor_relacao'];
		$cotacao->condutor_data_nascimento = Tools::converteData($data['condutor_data_nascimento']);
		$cotacao->condutor_sexo = $data['condutor_sexo'];
		$cotacao->condutor_estado_civil = $data['condutor_estado_civil'];
		$cotacao->condutor_num_carros = $data['condutor_num_carros'];
		$cotacao->garagem_residencia = $data['garagem_residencia'];
		$cotacao->garagem_residencia_tipo = $data['garagem_residencia_tipo'];
		$cotacao->garagem_trabalho = $data['garagem_trabalho'];
		$cotacao->garagem_trabalho_tipo = $data['garagem_trabalho_tipo'];
		$cotacao->garagem_escola = $data['garagem_escola'];
		$cotacao->garagem_escola_tipo = $data['garagem_escola_tipo'];
		$cotacao->cobertura_condutores_risco = $data['cobertura_condutores_risco'];
		$cotacao->observacoes = $data['observacoes'];

		$cotacao->save();

	
		Mail::send('emails.cotacaoAuto', $data, function($message) use ($data)
		{
		    $message->to('contato@corretoralexus.com.br', 'Lexus Corretora de Seguros')
		    		->subject('Novo pedido de cotação - Automóveis')
		    		->replyTo($data['email'], $data['nome']);

		    //if(Config::get('mail.pretend')) Log::info(View::make('emails.cotacaoAuto',$data)->render());
		});
		
		Session::flash('envioCotacao', true);	
		return Redirect::to('cotacao/auto');
	}

	public function outros()
	{
		$this->layout->with('templateCSS', array('css/cotacao'));
		$this->layout->content = view::make('frontend.cotacao.outros')->with('listaSegurosPessoal', SeguroPessoal::ordenado())
																	  ->with('listaSegurosEmpresarial', SeguroEmpresarial::ordenado());
	}

	public function gravarOutros()
	{
		$data['nome'] = Input::get('nome');
		$data['telefone'] = Input::get('telefone');
		$data['email'] = Input::get('email');
		$data['tipo_seguro'] = Input::get('tipo_seguro');
		$data['mensagem'] = Input::get('mensagem');
		
		$cotacao = new CotacoesOutras;
		$cotacao->nome = $data['nome'];
		$cotacao->telefone = $data['telefone'];
		$cotacao->email = $data['email'];
		$cotacao->tipo = $data['tipo_seguro'];
		$cotacao->texto = $data['mensagem'];

		$cotacao->save();

		if($data['nome'] && $data['email']){
			Mail::send('emails.cotacaoOutros', $data, function($message) use ($data)
			{
			    $message->to('corretoracontato@lexus.com.br', 'Lexus Corretora de Seguros')
			    		->subject('Novo pedido de cotação - Demais seguros')
			    		->replyTo($data['email'], $data['nome']);
			});

			//if(Config::get('mail.pretend')) Log::info(View::make('emails.cotacaoOutros',$data)->render());
		}
		
		Session::flash('envioCotacao', true);		
		return Redirect::to('cotacao/outros');	
	}

}
