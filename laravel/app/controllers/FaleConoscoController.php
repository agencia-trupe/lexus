<?php

use \FaleConosco, \Tools;

class FaleConoscoController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.faleconosco.index')->with('contato', FaleConosco::first());
	}

	public function enviar()
	{
		$data['nome'] = Input::get('nome');
		$data['email'] = Input::get('email');
		$data['telefone'] = Input::get('telefone');
		$data['mensagem'] = Input::get('mensagem');

		if($data['nome'] && $data['email'] && $data['mensagem']){
			Mail::send('emails.contato', $data, function($message) use ($data)
			{
			    $message->to('contato@lexus.com.br', 'Lexus Corretora de Seguros')
			    		->subject('Contato via site')
			    		->replyTo($data['email'], $data['nome']);
			});
		}
		
		Session::flash('envioContato', true);		
		return Redirect::to('fale-conosco');
	}
}
