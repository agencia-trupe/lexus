<?php

use \Banner, \Chamada, \ChamadaLateral, \Input, \Cadastro;

class HomeController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.home.index')->with('banners', Banner::ordenado())
																  ->with('chamadas', Chamada::all())
																  ->with('chamadaLateral', ChamadaLateral::first());
	}

	public function cadastro()
	{
		$nome = Input::get('nome');
		$email = Input::get('email');

		if(sizeof(Cadastro::where('email', '=', $email)->first()))
		{
			$erro = 'O e-mail informado já está cadastrado';
		}
		else
		{
			$erro = '0';

			$novo = new Cadastro;
			$novo->nome = $nome;
			$novo->email = $email;
			$novo->save();
		}

		return json_encode(array('erro' => $erro));
	}
}
