<?php

use \LinksUteis;

class LinksUteisController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.linksuteis.index')->with('linksUteis', LinksUteis::ordenado());
	}

}
