<?php

use \Parceiro;

class ParceirosController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.parceiros.index')->with('listaParceiros', Parceiro::ordenado());
	}

}
