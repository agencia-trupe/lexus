<?php

use \SeguroEmpresarial, \SeguroPessoal;

class SegurosController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function pessoais($slug = false)
	{
		$this->layout->with('templateCSS', 'css/seguros');
		if($slug)
		{
			$detalhe = SeguroPessoal::where('slug', '=', $slug)->first();
		}
		else
		{
			$detalhe = SeguroPessoal::orderBy('titulo', 'asc')->first();
		}

		$this->layout->content = View::make('frontend.seguros.index')->with('listaSeguros', SeguroPessoal::ordenado())
																	 ->with('pageTitle', 'SEGUROS PARA VOCÊ')
																	 ->with('slugArea', 'para-voce')
																	 ->with('diretorioImagens', 'seguropessoal')
																	 ->with('detalhe', $detalhe);
	}

	public function empresas($slug = false)
	{
		$this->layout->with('templateCSS', 'css/seguros');
		if($slug)
		{
			$detalhe = SeguroEmpresarial::where('slug', '=', $slug)->first();
		}
		else
		{
			$detalhe = SeguroEmpresarial::orderBy('titulo', 'asc')->first();
		}

		$this->layout->content = View::make('frontend.seguros.index')->with('listaSeguros', SeguroEmpresarial::ordenado())
																	 ->with('pageTitle', 'SEGUROS PARA EMPRESAS')
																	 ->with('slugArea', 'para-empresas')
																	 ->with('diretorioImagens', 'seguroempresarial')
																	 ->with('detalhe', $detalhe);
	}
}
