<?php

use \Sinistro, \ContatoSeguradora;

class SinistroController extends BaseController {

	protected $layout = 'frontend.templates.index';

	public function index()
	{
		$this->layout->content = View::make('frontend.sinistro.index')->with('textos', Sinistro::first())
																	  ->with('contatos', ContatoSeguradora::ordenado())
																	  ->with('num_contatos', sizeof(ContatoSeguradora::ordenado()));
	}

}
