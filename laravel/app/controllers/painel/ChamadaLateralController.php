<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, ChamadaLateral;

class ChamadaLateralController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = '1';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.chamadalateral.index')->with('registros', ChamadaLateral::all())->with('limiteInsercao', $this->limiteInsercao);		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.chamadalateral.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new ChamadaLateral;

		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');
		$object->texto_botao = Input::get('texto_botao');
		$object->link = Input::get('link');

		if($this->limiteInsercao && sizeof( ChamadaLateral::all() ) >= $this->limiteInsercao)
			return Redirect::back()->withErrors(array('Número máximo de Registros atingido!'));

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Chamada criada com sucesso.');
			return Redirect::route('painel.chamadalateral.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Chamada!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.chamadalateral.edit')->with('registro', ChamadaLateral::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = ChamadaLateral::find($id);

		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');
		$object->texto_botao = Input::get('texto_botao');
		$object->link = Input::get('link');


		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Chamada alterada com sucesso.');
			return Redirect::route('painel.chamadalateral.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Chamada!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = ChamadaLateral::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Chamada removida com sucesso.');

		return Redirect::route('painel.chamadalateral.index');
	}

}