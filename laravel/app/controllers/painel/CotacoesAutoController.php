<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, CotacoesAuto;

class CotacoesAutoController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.cotacoesauto.index')->with('registros', CotacoesAuto::paginate(25));		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.cotacoesauto.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new CotacoesAuto;

		$object->nome = Input::get('nome');
		$object->telefone = Input::get('telefone');
		$object->email = Input::get('email');
		$object->veiculo_modelo = Input::get('veiculo_modelo');

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Cotação criada com sucesso.');
			return Redirect::route('painel.cotacoesauto.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Cotação!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.cotacoesauto.edit')->with('registro', CotacoesAuto::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = CotacoesAuto::find($id);

		$object->nome = Input::get('nome');
		$object->telefone = Input::get('telefone');
		$object->email = Input::get('email');
		$object->veiculo_modelo = Input::get('veiculo_modelo');


		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Cotação alterada com sucesso.');
			return Redirect::route('painel.cotacoesauto.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Cotação!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = CotacoesAuto::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Cotação removida com sucesso.');

		return Redirect::route('painel.cotacoesauto.index');
	}

}