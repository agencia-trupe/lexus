<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, FaleConosco;

class FaleConoscoController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = '1';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.faleconosco.index')->with('registros', FaleConosco::paginate(25))->with('limiteInsercao', $this->limiteInsercao);		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.faleconosco.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new FaleConosco;

		$object->google_maps = Input::get('google_maps');
		$object->ddd = Input::get('ddd');
		$object->telefone = Input::get('telefone');
		$object->endereco = Input::get('endereco');
		$object->frase = Input::get('frase');
		$object->email = Input::get('email');
		$object->facebook = Input::get('facebook');
		$object->twitter = Input::get('twitter');
		$object->linkedin = Input::get('linkedin');

		if($this->limiteInsercao && sizeof( FaleConosco::all() ) >= $this->limiteInsercao)
			return Redirect::back()->withErrors(array('Número máximo de Registros atingido!'));

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto criado com sucesso.');
			return Redirect::route('painel.faleconosco.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Texto!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.faleconosco.edit')->with('registro', FaleConosco::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = FaleConosco::find($id);

		$object->google_maps = Input::get('google_maps');
		$object->ddd = Input::get('ddd');
		$object->telefone = Input::get('telefone');
		$object->endereco = Input::get('endereco');
		$object->frase = Input::get('frase');
		$object->email = Input::get('email');
		$object->facebook = Input::get('facebook');
		$object->twitter = Input::get('twitter');
		$object->linkedin = Input::get('linkedin');


		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto alterado com sucesso.');
			return Redirect::route('painel.faleconosco.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Texto!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = FaleConosco::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Texto removido com sucesso.');

		return Redirect::route('painel.faleconosco.index');
	}

}