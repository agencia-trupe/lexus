<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, LinksUteis;

class LinksUteisController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.linksuteis.index')->with('registros', LinksUteis::paginate(25));		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.linksuteis.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new LinksUteis;

		$object->titulo = Input::get('titulo');
		$object->texto_link = Input::get('texto_link');
		$object->link = Input::get('link');

		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Link criado com sucesso.');
			return Redirect::route('painel.linksuteis.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Link!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.linksuteis.edit')->with('registro', LinksUteis::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = LinksUteis::find($id);

		$object->titulo = Input::get('titulo');
		$object->texto_link = Input::get('texto_link');
		$object->link = Input::get('link');


		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Link alterado com sucesso.');
			return Redirect::route('painel.linksuteis.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Link!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = LinksUteis::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Link removido com sucesso.');

		return Redirect::route('painel.linksuteis.index');
	}

}