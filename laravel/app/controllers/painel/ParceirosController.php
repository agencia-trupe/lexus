<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, \Parceiro;

class ParceirosController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	protected $limiteInsercao = false;

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.parceiros.index')->with('registros', Parceiro::ordenado());
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.parceiros.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new Parceiro;

		$object->titulo = Input::get('titulo');

		$imagem = Thumb::make('imagem', 182, 142, 'parceiros/');
		if($imagem) $object->imagem = $imagem;
	
		try {

			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Parceiro criado com sucesso.');
			return Redirect::route('painel.parceiros.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Parceiro!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.parceiros.edit')->with('registro', Parceiro::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = Parceiro::find($id);

		$object->titulo = Input::get('titulo');

		$imagem = Thumb::make('imagem', 182, 142, 'parceiros/');
		if($imagem) $object->imagem = $imagem;
	
		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Parceiro alterado com sucesso.');
			return Redirect::route('painel.parceiros.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Parceiro!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = Parceiro::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Parceiro removido com sucesso.');

		return Redirect::route('painel.parceiros.index');
	}

}