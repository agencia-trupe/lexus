<?php

namespace Painel;

use \View, \Input, \Str, \Session, \Redirect, \Hash, \Thumb, \Tools, \File, SeguroPessoal;

class SeguroPessoalController extends BaseAdminController {

	protected $layout = 'backend.templates.index';

	public $limiteInsercao = '10';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->layout->content = View::make('backend.seguropessoal.index')
									->with('registros', SeguroPessoal::ordenado())
									->with('limiteInsercao', $this->limiteInsercao);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->layout->content = View::make('backend.seguropessoal.form');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$object = new SeguroPessoal;

		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');

		$imagem = Thumb::make('imagem', 630, 170, 'seguropessoal/');
		if($imagem) $object->imagem = $imagem;

		if($this->limiteInsercao && sizeof( SeguroPessoal::all() ) >= $this->limiteInsercao)
			return Redirect::back()->withErrors(array('Número máximo de Registros atingido!'));
		
		try {

			$object->save();

			$object->slug = Str::slug($object->id.' '.$object->titulo);
			$object->save();

			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto criado com sucesso.');
			return Redirect::route('painel.seguropessoal.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Texto!'));	

		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$this->layout->content = View::make('backend.seguropessoal.edit')->with('registro', SeguroPessoal::find($id));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$object = SeguroPessoal::find($id);

		$object->titulo = Input::get('titulo');
		$object->texto = Input::get('texto');
		$object->slug = Str::slug($object->id.' '.$object->titulo);

		$imagem = Thumb::make('imagem', 630, 170, 'seguropessoal/');
		if($imagem) $object->imagem = $imagem;

		try {

			$object->save();
			Session::flash('sucesso', true);
			Session::flash('mensagem', 'Texto alterado com sucesso.');
			return Redirect::route('painel.seguropessoal.index');

		} catch (\Exception $e) {

			Session::flash('formulario', Input::all());
			return Redirect::back()->withErrors(array('Erro ao criar Texto!'));

		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = SeguroPessoal::find($id);
		$object->delete();

		Session::flash('sucesso', true);
		Session::flash('mensagem', 'Texto removido com sucesso.');

		return Redirect::route('painel.seguropessoal.index');
	}

}