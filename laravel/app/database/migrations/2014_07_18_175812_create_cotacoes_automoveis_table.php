<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCotacoesAutomoveisTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cotacoes_automoveis', function(Blueprint $table)
		{
			$table->increments('id');
			
			$table->string('nome');
			$table->string('telefone');
			$table->string('email');
			
			$table->string('renovacao_seguradora');
			$table->string('renovacao_bonus');
			$table->string('renovacao_apolice');
			$table->string('renovacao_sinistro');
			$table->string('renovacao_vigencia');

			$table->string('segurado_nome');
			$table->string('segurado_cpfcnpj');
			$table->date('segurado_data_nascimento');
			$table->string('segurado_estado_civil');
			$table->string('segurado_residencia_cep');
			$table->string('segurado_residencia_endereco');
			$table->string('segurado_pernoite_cep');
			$table->string('segurado_pernoite_endereco');

			$table->string('veiculo_modelo');
			$table->string('veiculo_ano_fabricacao');
			$table->string('veiculo_ano_modelo');
			$table->string('veiculo_combustivel');
			$table->string('veiculo_placa');
			$table->string('veiculo_chassis');
			$table->string('veiculo_proprietario');

			$table->string('condutor_nome');
			$table->string('condutor_cpf');
			$table->string('condutor_relacao');
			$table->date('condutor_data_nascimento');
			$table->string('condutor_sexo');
			$table->string('condutor_estado_civil');
			$table->string('condutor_num_carros');

			$table->string('garagem_residencia');
			$table->string('garagem_residencia_tipo');
			$table->string('garagem_trabalho');
			$table->string('garagem_trabalho_tipo');
			$table->string('garagem_escola');
			$table->string('garagem_escola_tipo');
			$table->string('cobertura_condutores_risco');

			$table->text('observacoes');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cotacoes_automoveis');
	}

}
