<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contato', function(Blueprint $table)
		{
			$table->increments('id');
			$table->text('google_maps');
			$table->string('telefone');
			$table->string('endereco');
			$table->string('frase');
			$table->string('email');
			$table->string('facebook');
			$table->string('twitter');
			$table->string('linkedin');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contato');
	}

}
