<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterQuesomosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('quem_somos', function(Blueprint $table)
		{
			$table->dropColumn('texto');
			$table->text('texto_quemsomos')->after('id');
			$table->text('texto_missao')->after('texto_quemsomos');
			$table->text('texto_visao')->after('texto_missao');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('quem_somos', function(Blueprint $table)
		{
			$table->text('texto')->after('id');
			$table->dropColumn('texto_quemsomos');
			$table->dropColumn('texto_missao');
			$table->dropColumn('texto_visao');
		});
	}

}
