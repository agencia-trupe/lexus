<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSeguradorasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('contatos_seguradoras', function(Blueprint $table)
		{
			$table->string('imagem')->after('texto');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('contatos_seguradoras', function(Blueprint $table)
		{
			$table->dropColumn('imagem');
		});
	}

}
