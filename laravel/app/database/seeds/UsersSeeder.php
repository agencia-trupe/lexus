<?php

class UsersSeeder extends Seeder {

    public function run()
    {
        $data = array(
            array(
                'email' => 'contato@trupe.net',
                'username' => 'trupe',
                'password' => Hash::make('senhatrupe'),
            )
        );

        DB::table('usuarios')->insert($data);
    }

}
