<?php

Route::get('/', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('home', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('seguros/para-voce/{slug?}', array('as' => 'seguros.pessoais', 'uses' => 'SegurosController@pessoais'));
Route::get('seguros/para-empresas/{slug?}', array('as' => 'seguros.empresas', 'uses' => 'SegurosController@empresas'));
Route::get('parceiros', array('as' => 'parceiros', 'uses' => 'ParceirosController@index'));
Route::get('em-caso-de-sinistro', array('as' => 'sinistro', 'uses' => 'SinistroController@index'));
Route::get('quem-somos', array('as' => 'quemsomos', 'uses' => 'QuemSomosController@index'));
Route::get('links-uteis', array('as' => 'linksuteis', 'uses' => 'LinksUteisController@index'));
Route::get('fale-conosco', array('as' => 'faleconosco', 'uses' => 'FaleConoscoController@index'));
Route::post('fale-conosco', array('as' => 'faleconosco.post', 'uses' => 'FaleConoscoController@enviar'));
Route::get('cotacao', array('as' => 'cotacao.auto', 'uses' => 'CotacaoController@auto'));
Route::get('cotacao/auto', array('as' => 'cotacao.auto', 'uses' => 'CotacaoController@auto'));
Route::post('cotacao/auto', array('as' => 'cotacao.auto.gravar', 'uses' => 'CotacaoController@gravarAuto'));
Route::get('cotacao/outros', array('as' => 'cotacao.outros', 'uses' => 'CotacaoController@outros'));
Route::post('cotacao/outros', array('as' => 'cotacao.outros.gravar', 'uses' => 'CotacaoController@gravarOutros'));

Route::get('painel', array('before' => 'auth', 'as' => 'painel.home', 'uses' => 'Painel\HomeController@index'));
Route::get('painel/login', array('as' => 'painel.login', 'uses' => 'Painel\HomeController@login'));

Route::post('cadastroNewsletter', 'HomeController@cadastro');

// Autenticação do Login
Route::post('painel/login',  array('as' => 'painel.auth', function(){
	$authvars = array(
		'username' => Input::get('username'),
		'password' => Input::get('password')
	);

	$lembrar = (Input::get('lembrar') == '1') ? true : false;

	if(Auth::attempt($authvars, $lembrar)){
		return Redirect::to('painel');
	}else{
		Session::flash('login_errors', true);
		return Redirect::to('painel/login');
	}
}));

Route::get('painel/logout', array('as' => 'painel.off', function(){
	Auth::logout();
	return Redirect::to('painel');
}));

Route::post('ajax/gravaOrdem', array('before' => 'auth', 'uses' => 'Painel\AjaxController@gravaOrdem'));

Route::group(array('prefix' => 'painel', 'before' => 'auth'), function()
{

    Route::get('download', function(){

    	$filename = 'cadastros_'.date('d-m-Y-H-i-s');

		Excel::create($filename, function($excel) {

		    $excel->sheet('Usuários Cadastrados', function($sheet) {

		        $sheet->fromModel(\Cadastro::select('nome', 'email')->orderBy('nome')->get());

		    });

		})->download('csv');

    });

    Route::resource('usuarios', 'Painel\UsuariosController');
    Route::resource('chamadalateral', 'Painel\ChamadaLateralController');
	Route::resource('banners', 'Painel\BannersController');
	Route::resource('chamadas', 'Painel\ChamadasController');
	Route::resource('cadastros', 'Painel\CadastrosController');
	Route::resource('seguropessoal', 'Painel\SeguroPessoalController');
	Route::resource('seguroempresarial', 'Painel\SeguroEmpresarialController');
	Route::resource('cotacoesoutras', 'Painel\CotacoesOutrasController');
	Route::resource('parceiros', 'Painel\ParceirosController');
	Route::resource('sinistro', 'Painel\SinistroController');
	Route::resource('contatoseguradoras', 'Painel\ContatoSeguradorasController');
	Route::resource('quemsomos', 'Painel\QuemSomosController');
	Route::resource('linksuteis', 'Painel\LinksUteisController');
	Route::resource('faleconosco', 'Painel\FaleConoscoController');
	Route::resource('cotacoesauto', 'Painel\CotacoesAutoController');
//NOVASROTASDOPAINEL//
});