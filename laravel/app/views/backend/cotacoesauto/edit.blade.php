@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Cotação
        </h2>  

			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputNome">Nome</label>
					<input type="text" class="form-control" id="inputNome" name="nome" value="{{$registro->nome}}" disabled>
				</div>
				
				<div class="form-group">
					<label for="inputTelefone">Telefone</label>
					<input type="text" class="form-control" id="inputTelefone" name="telefone" value="{{$registro->telefone}}" disabled>
				</div>
				
				<div class="form-group">
					<label for="inputE-mail">E-mail</label>
					<input type="text" class="form-control" id="inputE-mail" name="email" value="{{$registro->email}}" disabled>
				</div>

				<hr>

				<div class="form-group">
					<label>Renovação - Seguradora</label>
					<input type="text" class="form-control" value="{{$registro->renovacao_seguradora}}" disabled>
				</div>

				<div class="form-group">
					<label>Renovação - Bônus descrito na Apólice</label>
					<input type="text" class="form-control" value="{{$registro->renovacao_bonus}}" disabled>
				</div>

				<div class="form-group">
					<label>Renovação - Número da Apólice</label>
					<input type="text" class="form-control" value="{{$registro->renovacao_apolice}}" disabled>
				</div>

				<div class="form-group">
					<label>Renovação - Houve Sinistro</label>
					<input type="text" class="form-control" value="{{$registro->renovacao_sinistro}}" disabled>
				</div>

				<div class="form-group">
					<label>Renovação - Vigência</label>
					<input type="text" class="form-control" value="{{$registro->renovacao_vigencia}}" disabled>
				</div>

				<hr>

				<div class="form-group">
					<label>Segurado - Nome</label>
					<input type="text" class="form-control" value="{{$registro->segurado_nome}}" disabled>
				</div>

				<div class="form-group">
					<label>Segurado - CPF ou CNPJ</label>
					<input type="text" class="form-control" value="{{$registro->segurado_cpfcnpj}}" disabled>
				</div>

				<div class="form-group">
					<label>Segurado - Data de Nascimento</label>
					<input type="text" class="form-control" value="{{Tools::converteData($registro->segurado_data_nascimento)}}" disabled>
				</div>

				<div class="form-group">
					<label>Segurado - Estado Civil</label>
					<input type="text" class="form-control" value="{{$registro->segurado_estado_civil}}" disabled>
				</div>

				<div class="form-group">
					<label>Segurado - CEP Residência</label>
					<input type="text" class="form-control" value="{{$registro->segurado_residencia_cep}}" disabled>
				</div>

				<div class="form-group">
					<label>Segurado - Endereço Residência</label>
					<input type="text" class="form-control" value="{{$registro->segurado_residencia_endereco}}" disabled>
				</div>

				<div class="form-group">
					<label>Segurado - CEP Pernoite</label>
					<input type="text" class="form-control" value="{{$registro->segurado_pernoite_cep}}" disabled>
				</div>

				<div class="form-group">
					<label>Segurado - Endereço Pernoite</label>
					<input type="text" class="form-control" value="{{$registro->segurado_pernoite_endereco}}" disabled>
				</div>
				
				<hr>

				<div class="form-group">
					<label>Veículo - Modelo Completo do Veículo</label>
					<input type="text" class="form-control" value="{{$registro->veiculo_modelo}}" disabled>
				</div>

				<div class="form-group">
					<label>Veículo - Ano Fabricação</label>
					<input type="text" class="form-control" value="{{$registro->veiculo_ano_fabricacao}}" disabled>
				</div>

				<div class="form-group">
					<label>Veículo - Ano Modelo</label>
					<input type="text" class="form-control" value="{{$registro->veiculo_ano_modelo}}" disabled>
				</div>

				<div class="form-group">
					<label>Veículo - Combustível</label>
					<input type="text" class="form-control" value="{{$registro->veiculo_combustivel}}" disabled>
				</div>

				<div class="form-group">
					<label>Veículo - Placa</label>
					<input type="text" class="form-control" value="{{$registro->veiculo_placa}}" disabled>
				</div>

				<div class="form-group">
					<label>Veículo - Chassis</label>
					<input type="text" class="form-control" value="{{$registro->veiculo_chassis}}" disabled>
				</div>

				<div class="form-group">
					<label>Veículo - Proprietário do Veículo</label>
					<input type="text" class="form-control" value="{{$registro->veiculo_proprietario}}" disabled>
				</div>

				<hr>

				<div class="form-group">
					<label>Condutor - Nome</label>
					<input type="text" class="form-control" value="{{$registro->condutor_nome}}" disabled>
				</div>

				<div class="form-group">
					<label>Condutor - CPF</label>
					<input type="text" class="form-control" value="{{$registro->condutor_cpf}}" disabled>
				</div>

				<div class="form-group">
					<label>Condutor - Relação com o Segurado</label>
					<input type="text" class="form-control" value="{{$registro->condutor_relacao}}" disabled>
				</div>

				<div class="form-group">
					<label>Condutor - Data de Nascimento</label>
					<input type="text" class="form-control" value="{{Tools::converteData($registro->condutor_data_nascimento)}}" disabled>
				</div>

				<div class="form-group">
					<label>Condutor - Sexo</label>
					<input type="text" class="form-control" value="{{$registro->condutor_sexo}}" disabled>
				</div>

				<div class="form-group">
					<label>Condutor - Estado Civil</label>
					<input type="text" class="form-control" value="{{$registro->condutor_estado_civil}}" disabled>
				</div>

				<div class="form-group">
					<label>Condutor - Quantos carros na Residência</label>
					<input type="text" class="form-control" value="{{$registro->condutor_num_carros}}" disabled>
				</div>

				<hr>

				<div class="form-group">
					<label>Garagem/Estacionamento - Na Residência</label>
					<input type="text" class="form-control" value="{{$registro->garagem_residencia}}" disabled>
				</div>

				<div class="form-group">
					<label>Garagem/Estacionamento - Portão Automático ou Porteiro</label>
					<input type="text" class="form-control" value="{{$registro->garagem_residencia_tipo}}" disabled>
				</div>

				<div class="form-group">
					<label>Garagem/Estacionamento - No Trabalho</label>
					<input type="text" class="form-control" value="{{$registro->garagem_trabalho}}" disabled>
				</div>

				<div class="form-group">
					<label>Garagem/Estacionamento - Portão Automático ou Porteiro</label>
					<input type="text" class="form-control" value="{{$registro->garagem_trabalho_tipo}}" disabled>
				</div>

				<div class="form-group">
					<label>Garagem/Estacionamento - Na Escola</label>
					<input type="text" class="form-control" value="{{$registro->garagem_escola}}" disabled>
				</div>

				<div class="form-group">
					<label>Garagem/Estacionamento - Portão Automático ou Porteiro</label>
					<input type="text" class="form-control" value="{{$registro->garagem_escola_tipo}}" disabled>
				</div>

				<hr>

				<div class="form-group">
					<label>Cobertura para Condutores de 18 a 25 anos</label>
					<input type="text" class="form-control" value="{{$registro->cobertura_condutores_risco}}" disabled>
				</div>

				<hr>

				<div class="form-group">
					<label>Observações</label>
					<div class="panel panel-default">
						<div class="panel-body">
					    	{{$registro->observacoes }}
					  	</div>
					</div>				
				</div>				

				<a href="{{URL::route('painel.cotacoesauto.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop