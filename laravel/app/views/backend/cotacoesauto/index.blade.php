@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Cotações - Seguro de Automóvel
    </h2>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Nome</th>
				<th>Telefone</th>
				<th>E-mail</th>
				<th>Veículo</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{ $registro->nome }}</td>
				<td>{{ $registro->telefone }}</td>
				<td>{{ $registro->email }}</td>
				<td>{{ $registro->veiculo_modelo.' - '.$registro->veiculo_ano_fabricacao.'/'.$registro->veiculo_ano_modelo }}</td>
                <td class="crud-actions" style="width:200px;">
                    <a href='{{ URL::route('painel.cotacoesauto.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>visualizar</a>
					{{ Form::open(array('route' => array('painel.cotacoesauto.destroy', $registro->id), 'method' => 'delete')) }}
						<button type='submit' class='btn btn-danger btn-sm btn-delete'>excluir</button>
					{{ Form::close() }}                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    {{ $registros->links() }}
</div>

@stop