@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Cotação
        </h2>  

	
		<div class="pad">

	    	@if(Session::has('sucesso'))
	    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
	        @endif

	    	@if($errors->any())
	    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
	    	@endif	


			<div class="form-group">
				<label for="inputNome">Nome</label>
				<input type="text" class="form-control" id="inputNome" name="nome" value="{{$registro->nome}}" disabled>
			</div>
			
			<div class="form-group">
				<label for="inputE-mail">E-mail</label>
				<input type="text" class="form-control" id="inputE-mail" name="email" value="{{$registro->email}}" disabled>
			</div>
			
			<div class="form-group">
				<label for="inputTelefone">Telefone</label>
				<input type="text" class="form-control" id="inputTelefone" name="telefone" value="{{$registro->telefone}}" disabled>
			</div>
			
			<div class="form-group">
				<label for="inputTipo">Tipo</label>
				<input type="text" class="form-control" id="inputTipo" name="tipo" value="{{$registro->tipo}}" disabled>
			</div>
			
			<div class="form-group">
				<label for="inputTexto">Texto</label>
				<div class="panel panel-default">
					<div class="panel-body">
				    	{{$registro->texto }}
				  	</div>
				</div>				
			</div>
			

			<a href="{{URL::route('painel.cotacoesoutras.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

		</div>
    </div>
    
@stop