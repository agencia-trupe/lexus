@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Cotação
        </h2>  

		<form action="{{URL::route('painel.cotacoesoutras.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputNome">Nome</label>
					<input type="text" class="form-control" id="inputNome" name="nome" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['nome'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputE-mail">E-mail</label>
					<input type="text" class="form-control" id="inputE-mail" name="email" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['email'] }}" @endif >
				</div>
				
				<div class="form-group">
					<label for="inputTelefone">Telefone</label>
					<input type="text" class="form-control" id="inputTelefone" name="telefone" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['telefone'] }}" @endif >
				</div>
				
				<div class="form-group">
					<label for="inputTipo">Tipo</label>
					<input type="text" class="form-control" id="inputTipo" name="tipo" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['tipo'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputTexto">Texto</label>
					<textarea name="texto" class="form-control" id="inputTexto" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['texto'] }} @endif</textarea>
				</div>
				

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.cotacoesoutras.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop