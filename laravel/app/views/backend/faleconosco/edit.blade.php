@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Informações de Contato
        </h2>  

		{{ Form::open( array('route' => array('painel.faleconosco.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputGoogle Maps">Google Maps</label>
					<input type="text" class="form-control" id="inputGoogle Maps" name="google_maps" value='{{$registro->google_maps}}'>
				</div>
				
				<label>DDD / Telefone</label>
				<div class="form-inline">
					<div class="form-group">
						<input type="text" id="inputDDD" style="width:40px" maxlength="2" class="form-control" name="ddd" value="{{$registro->ddd}}"> 
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="inputTelefone" name="telefone" value="{{$registro->telefone}}">
					</div>
				</div><br>
				
				<div class="form-group">
					<label for="inputEndereço">Endereço</label>
					<textarea name="endereco" class="form-control" id="inputEndereço" >{{$registro->endereco }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputFrase">Frase</label>
					<textarea name="frase" class="form-control" id="inputFrase" >{{$registro->frase }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputEmail">Email</label>
					<input type="text" class="form-control" id="inputEmail" name="email" value="{{$registro->email}}" required>
				</div>
				
				<div class="form-group">
					<label for="inputFacebook">Facebook</label>
					<input type="text" class="form-control" id="inputFacebook" name="facebook" value="{{$registro->facebook}}" >
				</div>
				
				<div class="form-group">
					<label for="inputTwitter">Twitter</label>
					<input type="text" class="form-control" id="inputTwitter" name="twitter" value="{{$registro->twitter}}" >
				</div>
				
				<div class="form-group">
					<label for="inputLinkedIn">LinkedIn</label>
					<input type="text" class="form-control" id="inputLinkedIn" name="linkedin" value="{{$registro->linkedin}}" >
				</div>
				

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.faleconosco.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop