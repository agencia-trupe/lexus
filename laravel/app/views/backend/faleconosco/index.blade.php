@section('conteudo')

<div class="container">

    @if(Session::has('sucesso'))
        <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
    @endif

    @if($errors->any())
        <div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
    @endif

    <h2>
        Fale Conosco 
    </h2>

    <table class='table table-striped table-bordered table-hover'>

        <thead>
            <tr>
                <th>Telefone</th>
				<th>Endereço</th>
				<th>Email</th>
                <th>Mídias Sociais</th>				
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)

            <tr class="tr-row" id="row_{{ $registro->id }}">
                <td>{{ $registro->ddd .' '. $registro->telefone }}</td>
				<td>{{ Str::words(strip_tags($registro->endereco), 15) }}</td>
				<td>{{ $registro->email }}</td>
				<td>
                    @if($registro->facebook)
                        <a href="{{ $registro->facebook }}" title="Facebook" target="_blank" class="btn btn-default btn-sm">Facebook</a>
                    @endif
                    @if($registro->twitter)
                        <a href="{{ $registro->twitter }}" title="Twitter" target="_blank" class="btn btn-default btn-sm">Twitter</a>
                    @endif
                    @if($registro->linkedin)
                        <a href="{{ $registro->linkedin }}" title="LinkedIn" target="_blank" class="btn btn-default btn-sm">LinkedIn</a>                    
                    @endif
                </td>
                <td class="crud-actions">
                    <a href='{{ URL::route('painel.faleconosco.edit', $registro->id ) }}' class='btn btn-primary btn-sm'>editar</a>                    
                </td>
            </tr>

        @endforeach
        </tbody>

    </table>

    {{ $registros->links() }}
</div>

@stop