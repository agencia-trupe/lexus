@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Link
        </h2>  

		<form action="{{URL::route('painel.linksuteis.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['titulo'] }}" @endif required>
				</div>
				
				<div class="form-group">
					<label for="inputDescrição">Descrição</label>
					<input type="text" class="form-control" id="inputDescrição" name="texto_link" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['texto_link'] }}" @endif >
				</div>
				
				<div class="form-group">
					<label for="inputLink">Link</label>
					<input type="text" class="form-control" id="inputLink" name="link" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['link'] }}" @endif required>
				</div>
				

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.linksuteis.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop