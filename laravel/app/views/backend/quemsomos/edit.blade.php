@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Texto
        </h2>  

		{{ Form::open( array('route' => array('painel.quemsomos.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	


				<div class="form-group">
					<label for="inputTextoQuemSomos">Texto - Quem Somos</label>
					<textarea name="texto_quemsomos" class="form-control" id="inputTextoQuemSomos">{{$registro->texto_quemsomos }}</textarea>
				</div>

				<div class="form-group">
					<label for="inputTextoMissao">Texto - Missão</label>
					<textarea name="texto_missao" class="form-control" id="inputTextoMissao">{{$registro->texto_missao }}</textarea>
				</div>

				<div class="form-group">
					<label for="inputTextoVisao">Texto - Visão</label>
					<textarea name="texto_visao" class="form-control" id="inputTextoVisao">{{$registro->texto_visao }}</textarea>
				</div>
				

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.quemsomos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop