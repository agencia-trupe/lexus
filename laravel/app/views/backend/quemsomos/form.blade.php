@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Texto
        </h2>  

		<form action="{{URL::route('painel.quemsomos.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif


				<div class="form-group">
					<label for="inputTextoQuemSomos">Texto - Quem Somos</label>
					<textarea name="texto_quemsomos" class="form-control" id="inputTextoQuemSomos">@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['texto_quemsomos'] }} @endif</textarea>
				</div>

				<div class="form-group">
					<label for="inputTextoMissao">Texto - Missão</label>
					<textarea name="texto_missao" class="form-control" id="inputTextoMissao">@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['texto_missao'] }} @endif</textarea>
				</div>

				<div class="form-group">
					<label for="inputTextoVisao">Texto - Visão</label>
					<textarea name="texto_visao" class="form-control" id="inputTextoVisao">@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['texto_visao'] }} @endif</textarea>
				</div>
				

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.quemsomos.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop