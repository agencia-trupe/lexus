@section('conteudo')

    <div class="container add">

      	<h2>
        	Editar Texto
        </h2>  

		{{ Form::open( array('route' => array('painel.sinistro.update', $registro->id), 'files' => true, 'method' => 'put') ) }}
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif	

				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" value="{{$registro->titulo}}" required>
				</div>

				<div class="form-group">
					<label for="inputTexto - Coluna Esquerda">Texto - Coluna Esquerda</label>
					<textarea name="texto_esquerda" class="form-control" id="inputTexto - Coluna Esquerda" >{{$registro->texto_esquerda }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputTexto - Coluna Direita">Texto - Coluna Direita</label>
					<textarea name="texto_direita" class="form-control" id="inputTexto - Coluna Direita" >{{$registro->texto_direita }}</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputTexto - Inferior">Texto - Inferior</label>
					<textarea name="aviso" class="form-control" id="inputTexto - Inferior" >{{$registro->aviso }}</textarea>
				</div>
				

				<button type="submit" title="Alterar" class="btn btn-success">Alterar</button>

				<a href="{{URL::route('painel.sinistro.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop