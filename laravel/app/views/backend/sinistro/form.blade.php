@section('conteudo')

    <div class="container add">

      	<h2>
        	Adicionar Texto
        </h2>  

		<form action="{{URL::route('painel.sinistro.store')}}" method="post" enctype="multipart/form-data">
			<div class="pad">

		    	@if(Session::has('sucesso'))
		    	   <div class="alert alert-block alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ Session::get('mensagem') }}</div>
		        @endif

		    	@if($errors->any())
		    		<div class="alert alert-block alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>{{ $errors->first() }}</div>
		    	@endif

				<div class="form-group">
					<label for="inputTítulo">Título</label>
					<input type="text" class="form-control" id="inputTítulo" name="titulo" @if(Session::has('formulario')) value="{{ $s=Session::get('formulario'); echo $s['titulo'] }}" @endif required>
				</div>

				<div class="form-group">
					<label for="inputTexto - Coluna Esquerda">Texto - Coluna Esquerda</label>
					<textarea name="texto_esquerda" class="form-control" id="inputTexto - Coluna Esquerda" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['texto_esquerda'] }} @endif</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputTexto - Coluna Direita">Texto - Coluna Direita</label>
					<textarea name="texto_direita" class="form-control" id="inputTexto - Coluna Direita" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['texto_direita'] }} @endif</textarea>
				</div>
				
				<div class="form-group">
					<label for="inputTexto - Inferior">Texto - Inferior</label>
					<textarea name="aviso" class="form-control" id="inputTexto - Inferior" >@if(Session::has('formulario')) {{ $s=Session::get('formulario'); echo $s['aviso'] }} @endif</textarea>
				</div>
				

				<button type="submit" title="Inserir" class="btn btn-success">Inserir</button>

				<a href="{{URL::route('painel.sinistro.index')}}" title="Voltar" class="btn btn-default btn-voltar">Voltar</a>

			</div>
		</form>
    </div>
    
@stop