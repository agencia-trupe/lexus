<!DOCTYPE html>
<html lang="pt-BR" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, nofollow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=device-width,initial-scale=1">
    
    <title>Lexus - Painel Administrativo</title>
	<meta name="description" content="">
    <meta name="keywords" content="" />
    
	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>

	<?=Assets::CSS(array(
		'vendor/reset-css/reset',
		'css/fontface/stylesheet',
		'vendor/jquery-ui/themes/base/jquery-ui',
		'vendor/bootstrap/dist/css/bootstrap.min',
		'css/painel/painel'
	))?>

	@if(isset($css))
		<?=Assets::CSS(array($css))?>
	@endif

	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="assets/vendor/jquery/dist/jquery.min.js"><\/script>')</script>

	@if(App::environment()=='local')
		<?=Assets::JS(array('js/vendor/modernizr/modernizr', 'vendor/less.js/dist/less-1.6.2.min'))?>
	@else
		<?=Assets::JS(array('js/vendor/modernizr/modernizr'))?>
	@endif

</head>
	<body @if(Route::currentRouteName() == 'painel.login') class="body-painel-login" @else class="body-painel" @endif >

		@if(Route::currentRouteName() != 'painel.login')

			<nav class="navbar navbar-default">
				<div class="navbar-inner">
					<a href="{{URL::route('painel.home')}}" title="Página Inicial" class="navbar-brand">Lexus</a>

					<ul class="nav navbar-nav">

						<li @if(str_is('painel.home*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.home')}}" title="Início">Início</a>
						</li>

						<li class="dropdown @if(str_is('painel.chamada*', Route::currentRouteName()) || str_is('painel.banners*', Route::currentRouteName())) active @endif ">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Home <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.chamadalateral.index')}}" title="Chamada Lateral">Chamada Lateral</a><li>
								<li><a href="{{URL::route('painel.banners.index')}}" title="Banners">Banners</a><li>
								<li><a href="{{URL::route('painel.chamadas.index')}}" title="Chamadas">Chamadas</a><li>
							</ul>
						</li>

						<li class="dropdown @if(str_is('painel.seguro*', Route::currentRouteName())) active @endif ">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Seguros <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.seguropessoal.index')}}" title="Pessoais">para Você</a><li>
								<li><a href="{{URL::route('painel.seguroempresarial.index')}}" title="Empresariais">para Empresas</a><li>
							</ul>
						</li>

						<li class="dropdown @if(str_is('painel.cotacoes*', Route::currentRouteName())) active @endif ">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Pedidos de Cotação <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.cotacoesauto.index')}}" title="Automóveis">Automóveis</a><li>
								<li><a href="{{URL::route('painel.cotacoesoutras.index')}}" title="Demais Seguros">Demais Seguros</a><li>
							</ul>
						</li>

						<li @if(str_is('painel.parceiros*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.parceiros.index')}}" title="Parceiros">Parceiros</a>
						</li>

						<li class="dropdown @if(str_is('painel.sinistro*', Route::currentRouteName()) || str_is('painel.contatoseguradoras*', Route::currentRouteName())) active @endif ">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Em caso de Sinistro <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.sinistro.index')}}" title="Textos">Textos</a><li>
								<li><a href="{{URL::route('painel.contatoseguradoras.index')}}" title="Info. de Contato das Seguradoras">Info. de Contato das Seguradoras</a><li>
							</ul>
						</li>

						<li @if(str_is('painel.quemsomos*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.quemsomos.index')}}" title="Quem Somos">Quem Somos</a>
						</li>

						<li @if(str_is('painel.linksuteis*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.linksuteis.index')}}" title="Link Úteis">Link Úteis</a>
						</li>

						<li @if(str_is('painel.faleconosco*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.faleconosco.index')}}" title="Link Úteis">Fale Conosco</a>
						</li>

						<li @if(str_is('painel.cadastros*', Route::currentRouteName())) class="active" @endif >
							<a href="{{URL::route('painel.cadastros.index')}}" title="Cadastros de Newsletter">Cadastros de Newsletter</a>
						</li>

						<li class="dropdown @if(str_is('painel.usuarios*', Route::currentRouteName())) active @endif ">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="{{URL::route('painel.usuarios.index')}}" title="Usuários do Painel">Usuários do Painel</a></li>
								<li><a href="{{URL::route('painel.off')}}" title="Logout">Logout</a></li>
							</ul>
						</li>

					</ul>
				</div>
			</nav>

		@endif

		<div class="main {{ 'main-'.str_replace('.', '-', Route::currentRouteName()) }}">
			@yield('conteudo')
		</div>
		
		<footer>
			<div class="container">
				<a href="http://www.trupe.net" target="_blank" title="Criação de Sites : Trupe Agência Criativa">© Criação de Sites : Trupe Agência Criativa</a>
			</div>
		</footer>

		@if(Route::currentRouteName() == 'painel.login')
			<?=Assets::JS(array(
				'vendor/jquery-ui/ui/minified/jquery-ui.min',
				'vendor/bootbox/bootbox',
				'vendor/ckeditor/ckeditor',
				'vendor/ckeditor/adapters/jquery',
				'vendor/bootstrap/dist/js/bootstrap.min',
				'js/painel/login'
			))?>
		@else
			<?=Assets::JS(array(
				'vendor/jquery-ui/ui/minified/jquery-ui.min',
				'vendor/bootbox/bootbox',
				'vendor/ckeditor/ckeditor',
				'vendor/ckeditor/adapters/jquery',
				'vendor/bootstrap/dist/js/bootstrap.min',
				'js/painel/painel'
			))?>
		@endif

	</body>
</html>
