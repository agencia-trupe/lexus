<!DOCTYPE html>
<html>
<head>
    <title>Mensagem de contato via formulário do site</title>
    <meta charset="utf-8">
</head>
<body>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Nome :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$nome}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Telefone :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$telefone}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>E-mail :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$email}} </span><br>
	<hr>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Renovação - Seguradora :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$renovacao_seguradora}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Renovação - Bônus descrito na Apólice :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$renovacao_bonus}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Renovação - Número da Apólice :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$renovacao_apolice}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Renovação - Houve Sinistro :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$renovacao_sinistro}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Renovação - Vigência :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$renovacao_vigencia}} </span><br>
	<hr>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Segurado - Nome :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$segurado_nome}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Segurado - CPF ou CNPJ :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$segurado_cpfcnpj}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Segurado - Data de Nascimento :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$segurado_data_nascimento}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Segurado - Estado Civil :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$segurado_estado_civil}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Segurado - CEP Residência :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$segurado_residencia_cep}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Segurado - Endereço Residência :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$segurado_residencia_endereco}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Segurado - CEP Pernoite :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$segurado_pernoite_cep}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Segurado - Endereço Pernoite :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$segurado_pernoite_endereco}} </span><br>
	<hr>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Veículo - Modelo Completo do Veículo :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$veiculo_modelo}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Veículo - Ano Fabricação :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$veiculo_ano_fabricacao}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Veículo - Ano Modelo :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$veiculo_ano_modelo}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Veículo - Combustível :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$veiculo_combustivel}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Veículo - Placa :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$veiculo_placa}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Veículo - Chassis :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$veiculo_chassis}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Veículo - Proprietário do Veículo :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$veiculo_proprietario}} </span><br>
	<hr>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Condutor - Nome :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$condutor_nome}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Condutor - CPF :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$condutor_cpf}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Condutor - Relação com o Segurado :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$condutor_relacao}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Condutor - Data de Nascimento :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$condutor_data_nascimento}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Condutor - Sexo :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$condutor_sexo}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Condutor - Estado Civil :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$condutor_estado_civil}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Condutor - Quantos carros na Residência :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$condutor_num_carros}} </span><br>
	<hr>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Garagem/Estacionamento - Na Residência :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$garagem_residencia}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Garagem/Estacionamento - Portão Automático ou Porteiro :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$garagem_residencia_tipo}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Garagem/Estacionamento - No Trabalho :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$garagem_trabalho}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Garagem/Estacionamento - Portão Automático ou Porteiro :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$garagem_trabalho_tipo}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Garagem/Estacionamento - Na Escola :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$garagem_escola}} </span><br>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Garagem/Estacionamento - Portão Automático ou Porteiro :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$garagem_escola_tipo}} </span><br>
	<hr>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Cobertura para Condutores de 18 a 25 anos :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$cobertura_condutores_risco}} </span><br>
	<hr>
	<span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Observações :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'> {{$observacoes}} </span><br>
</body>
</html>
