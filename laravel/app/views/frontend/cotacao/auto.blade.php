@section('conteudo')

	<h1 class="pagetitle">
		<div class="centro">
			SOLICITE UMA COTAÇÃO
		</div>
	</h1>

	<div class="centro">
		<div class="internal-padding">

			<ul id="cotacoes-nav">
				<li><a href="cotacao/auto" title="SEGURO DE AUTOMÓVEIS" @if(str_is('cotacao.auto', Route::currentRouteName())) class='ativo' @endif>SEGURO DE AUTOMÓVEIS</a></li>
				<li><a href="cotacao/outros" title="DEMAIS SEGUROS" @if(str_is('cotacao.outros', Route::currentRouteName())) class='ativo' @endif>DEMAIS SEGUROS</a></li>
			</ul>


			<div class="form-auto">
				@if(Session::has('erroEnvioCotacao'))
					<script>
						alert("{{ Session::get('erroEnvioCotacao') }}");
						console.log("{{ Session::get('erroEnvioCotacao') }}");
					</script>
				@endif

				@if(Session::has('envioCotacao'))
					
					<h1 id="pagenotfound" style='margin:10px 0; line-height:140%;'>Solicitação de Cotação enviada com sucesso!<br>Retornaremos assim que possível.</h1>

				@else

					<form action="{{URL::route('cotacao.auto.gravar')}}" id="formCotacaoAuto" method="post" novalidate>
						
						<div class="secao-form">
							<h2>DADOS GERAIS</h2>

							<div class="secao-interna">
								
								<label class="full mbottom">
									Nome<br>
									<input @if(Session::has('formularioAuto')) value="{{Session::get('formularioAuto')['nome']}}" @endif type="text" name="nome" id="formNome" required>
								</label>

								<label class="w270">
									Telefone<br>
									<input @if(Session::has('formularioAuto')) value="{{Session::get('formularioAuto')['telefone']}}" @endif type="text" name="telefone" id="formTelefone" required>
								</label>

								<label class="w390 mleft">
									E-mail<br>
									<input @if(Session::has('formularioAuto')) value="{{Session::get('formularioAuto')['email']}}" @endif type="email" name="email" id="formEmail" required>
								</label>

							</div>

							<div class="secao-interna">
								
								<h3>SE RENOVAÇÃO, INFORME:</h3>

								<label class="w440 mbottom">
									Seguradora<br>
									<input @if(Session::has('formularioAuto')) value="{{Session::get('formularioAuto')['renovacao_seguradora']}}" @endif type="text" name="renovacao_seguradora" id="formRenovacao_seguradora">
								</label>

								<label class="w220 mleft">
									Bônus descrito na Apólice<br>
									<select name="renovacao_bonus" id="formRenovacao_bonus">
										<option value=""></option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['renovacao_bonus'] == 'Classe 1') selected @endif value="Classe 1">Classe 1</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['renovacao_bonus'] == 'Classe 2') selected @endif value="Classe 2">Classe 2</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['renovacao_bonus'] == 'Classe 3') selected @endif value="Classe 3">Classe 3</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['renovacao_bonus'] == 'Classe 4') selected @endif value="Classe 4">Classe 4</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['renovacao_bonus'] == 'Classe 5') selected @endif value="Classe 5">Classe 5</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['renovacao_bonus'] == 'Classe 6') selected @endif value="Classe 6">Classe 6</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['renovacao_bonus'] == 'Classe 7') selected @endif value="Classe 7">Classe 7</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['renovacao_bonus'] == 'Classe 8') selected @endif value="Classe 8">Classe 8</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['renovacao_bonus'] == 'Classe 9') selected @endif value="Classe 9">Classe 9</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['renovacao_bonus'] == 'Classe 10') selected @endif value="Classe 10">Classe 10</option>
									</select>
								</label>

								<label class="w440 mbottom">
									Número da Apólice<br>
									<input @if(Session::has('formularioAuto')) value="{{Session::get('formularioAuto')['renovacao_apolice']}}" @endif type="text" name="renovacao_apolice" id="formRenovacao_apolice">
								</label>

								<label class="w220 mleft">
									Houve Sinistro<br>
									<select name="renovacao_sinistro" id="formRenovacao_sinistro">
										<option value=""></option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['renovacao_sinistro'] == 'Sim') selected @endif value="Sim">Sim</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['renovacao_sinistro'] == 'Não') selected @endif value="Não">Não</option>
									</select>
								</label>

								<label class="w440">
									Vigência<br>
									<input @if(Session::has('formularioAuto')) value="{{Session::get('formularioAuto')['renovacao_vigencia']}}" @endif type="text" name="renovacao_vigencia" id="formRenovacao_vigencia">
								</label>

							</div>
						</div>

						<div class="secao-form">
							<h2>SEGURADO</h2>

							<div class="secao-interna">
								
								<label class="full mbottom">
									Nome<br>
									<input @if(Session::has('formularioAuto')) value="{{Session::get('formularioAuto')['segurado_nome']}}" @endif type="text" name="segurado_nome" id="formSegurado_nome" required>
								</label>

								<label class="w200">
									CPF ou CNPJ<br>
									<input @if(Session::has('formularioAuto')) value="{{Session::get('formularioAuto')['segurado_cpfcnpj']}}" @endif type="text" maxlength="18" name="segurado_cpfcnpj" id="formSegurado_cpfcnpj" required>
								</label>

								<label class="w200 mleft">
									Data de Nascimento<br>
									<input @if(Session::has('formularioAuto')) value="{{Session::get('formularioAuto')['segurado_data_nascimento']}}" @endif type="text" name="segurado_data_nascimento" id="formSegurado_data_nascimento" required>
								</label>

								<label class="w240 mleft">
									Estado Civil<br>
									<select name="segurado_estado_civil" id="formSegurado_estado_civil" required>
										<option value=""></option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['segurado_estado_civil'] == 'Solteiro(a)') selected @endif value="Solteiro(a)">Solteiro(a)</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['segurado_estado_civil'] == 'Casado ou reside há pelo menos 2 anos com companheira(o)') selected @endif value="Casado ou reside há pelo menos 2 anos com companheira(o)">Casado ou reside há pelo menos 2 anos com companheira(o)</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['segurado_estado_civil'] == 'Separado(a)/Divorciado(a)') selected @endif value="Separado(a)/Divorciado(a)">Separado(a)/Divorciado(a)</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['segurado_estado_civil'] == 'Viúvo(a)') selected @endif value="Viúvo(a)">Viúvo(a)</option>
									</select>
								</label>

							</div>

							<div class="secao-interna">
								
								<label class="w200 mbottom">
									CEP Residência<br>
									<input @if(Session::has('formularioAuto')) value="{{Session::get('formularioAuto')['segurado_residencia_cep']}}" @endif type="text" name="segurado_residencia_cep" id="formSegurado_residencia_cep" required>
								</label>

								<label class="w460 mleft mbottom">
									Endereço Residência<br>
									<input @if(Session::has('formularioAuto')) value="{{Session::get('formularioAuto')['segurado_residencia_endereco']}}" @endif type="text" name="segurado_residencia_endereco" id="formSegurado_residencia_endereco" required>
								</label>

								<label class="w200">
									CEP Pernoite<br>
									<input @if(Session::has('formularioAuto')) value="{{Session::get('formularioAuto')['segurado_pernoite_cep']}}" @endif type="text" name="segurado_pernoite_cep" id="formSegurado_pernoite_cep" required>
								</label>

								<label class="w460 mleft">
									Endereço Pernoite<br>
									<input @if(Session::has('formularioAuto')) value="{{Session::get('formularioAuto')['segurado_pernoite_endereco']}}" @endif type="text" name="segurado_pernoite_endereco" id="formSegurado_pernoite_endereco" required>
								</label>

							</div>

						</div>

						<div class="secao-form">
							<h2>VEÍCULO</h2>

							<div class="secao-interna">

								<label class="full mbottom">
									Modelo Completo do Veículo<br>
									<input @if(Session::has('formularioAuto')) value="{{Session::get('formularioAuto')['veiculo_modelo']}}" @endif type="text" name="veiculo_modelo" id="formVeiculo_modelo" required>
								</label>

								<label class="w200 mbottom">
									Ano Fabricação<br>
									<input @if(Session::has('formularioAuto')) value="{{Session::get('formularioAuto')['veiculo_ano_fabricacao']}}" @endif type="text" name="veiculo_ano_fabricacao" id="formVeiculo_ano_fabricacao" required>
								</label>

								<label class="w200 mleft">
									Ano Modelo<br>
									<input @if(Session::has('formularioAuto')) value="{{Session::get('formularioAuto')['veiculo_ano_modelo']}}" @endif type="text" name="veiculo_ano_modelo" id="formVeiculo_ano_modelo" required>
								</label>

								<label class="w240 mleft">
									Combustível<br>
									<select name="veiculo_combustivel" id="formVeiculo_combustivel" required>
										<option value=""></option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['veiculo_combustivel'] == 'Álcool') selected @endif value="Álcool">Álcool</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['veiculo_combustivel'] == 'Gasolina') selected @endif value="Gasolina">Gasolina</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['veiculo_combustivel'] == 'Flex') selected @endif value="Flex">Flex</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['veiculo_combustivel'] == 'Diesel') selected @endif value="Diesel">Diesel</option>
									</select>
								</label>

								<label class="w200 mbottom">
									Placa<br>
									<input @if(Session::has('formularioAuto')) value="{{Session::get('formularioAuto')['veiculo_placa']}}" @endif type="text" name="veiculo_placa" id="formVeiculo_placa" required>
								</label>

								<label class="w200 mleft">
									Chassis<br>
									<input @if(Session::has('formularioAuto')) value="{{Session::get('formularioAuto')['veiculo_chassis']}}" @endif type="text" name="veiculo_chassis" id="formVeiculo_chassis" required>
								</label>

								<label class="full">
									Proprietário do Veículo<br>
									<input @if(Session::has('formularioAuto')) value="{{Session::get('formularioAuto')['veiculo_proprietario']}}" @endif type="text" name="veiculo_proprietario" id="formVeiculo_proprietario" required>
								</label>

							</div>

						</div>


						<div class="secao-form">
							<h2>CONDUTOR PRINCIPAL</h2>

							<div class="secao-interna">
								
								<label class="full mbottom">
									Nome<br>
									<input @if(Session::has('formularioAuto')) value="{{Session::get('formularioAuto')['condutor_nome']}}" @endif type="text" name="condutor_nome" id="formCondutor_nome" required>
								</label>

								<label class="w200 mbottom">
									CPF<br>
									<input @if(Session::has('formularioAuto')) value="{{Session::get('formularioAuto')['condutor_cpf']}}" @endif type="text" name="condutor_cpf" id="formCondutor_cpf" required>
								</label>

								<label class="w240 mleft">
									Relação com o Segurado<br>
									<select name="condutor_relacao" id="formCondutor_relacao" required>
										<option value=""></option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['condutor_relacao'] == 'O Próprio') selected @endif value="O Próprio">O Próprio</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['condutor_relacao'] == 'Filho (a)') selected @endif value="Filho (a)">Filho (a)</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['condutor_relacao'] == 'Cônjuge') selected @endif value="Cônjuge">Cônjuge</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['condutor_relacao'] == 'Motorista Particular') selected @endif value="Motorista Particular">Motorista Particular</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['condutor_relacao'] == 'Diretor/Gerente/Sócio') selected @endif value="Diretor/Gerente/Sócio">Diretor/Gerente/Sócio</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['condutor_relacao'] == 'Pai/Mãe') selected @endif value="Pai/Mãe">Pai/Mãe</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['condutor_relacao'] == 'Funcionário/Empregado') selected @endif value="Funcionário/Empregado">Funcionário/Empregado</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['condutor_relacao'] == 'Outros') selected @endif value="Outros">Outros</option>
									</select>
								</label>

								<label class="w200 mleft">
									Data de Nascimento<br>
									<input @if(Session::has('formularioAuto')) value="{{Session::get('formularioAuto')['condutor_data_nascimento']}}" @endif type="text" name="condutor_data_nascimento" id="formCondutor_data_nascimento" required>
								</label>

								<label class="w200">
									Sexo<br>
									<select name="condutor_sexo" id="formCondutor_sexo" required>
										<option value=""></option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['condutor_sexo'] == 'Masculino') selected @endif value="Masculino">Masculino</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['condutor_sexo'] == 'Feminino') selected @endif value="Feminino">Feminino</option>
									</select>
								</label>

								<label class="w240 mleft">
									Estado Civil<br>
									<select name="condutor_estado_civil" id="formCondutor_estado_civil" required>
										<option value=""></option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['condutor_estado_civil'] == 'Solteiro(a)') selected @endif value="Solteiro(a)">Solteiro(a)</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['condutor_estado_civil'] == 'Casado ou reside há pelo menos 2 anos com companheira(o)') selected @endif value="Casado ou reside há pelo menos 2 anos com companheira(o)">Casado ou reside há pelo menos 2 anos com companheira(o)</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['condutor_estado_civil'] == 'Separado(a)/Divorciado(a)') selected @endif value="Separado(a)/Divorciado(a)">Separado(a)/Divorciado(a)</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['condutor_estado_civil'] == 'Viúvo(a)') selected @endif value="Viúvo(a)">Viúvo(a)</option>
									</select>
								</label>

								<label class="w200 mbottom mleft">
									Quantos carros na Residência<br>
									<input @if(Session::has('formularioAuto')) value="{{Session::get('formularioAuto')['condutor_num_carros']}}" @endif type="text" name="condutor_num_carros" id="formCondutor_num_carros" required>
								</label>

							</div>

							<div class="secao-interna">

								<h3>GARAGEM OU ESTACIONAMENTO:</h3>

								<label class="w200">
									Na Residência<br>
									<select name="garagem_residencia" id="formGaragem_residencia" required>
										<option value=""></option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['garagem_residencia'] == 'Sim') selected @endif value="Sim">Sim</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['garagem_residencia'] == 'Não') selected @endif value="Não">Não</option>
									</select>
								</label>

								<label class="w200 mbottom mleft">
									Portão Automático ou Porteiro<br>
									<select name="garagem_residencia_tipo" id="formGaragem_residencia_tipo" required>
										<option value=""></option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['garagem_residencia_tipo'] == 'Sim') selected @endif value="Sim">Sim</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['garagem_residencia_tipo'] == 'Não') selected @endif value="Não">Não</option>
									</select>
								</label>

								<br>

								<label class="w200 mbottom">
									No Trabalho<br>
									<select name="garagem_trabalho" id="formGaragem_trabalho" required>
										<option value=""></option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['garagem_trabalho'] == 'Sim') selected @endif value="Sim">Sim</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['garagem_trabalho'] == 'Não') selected @endif value="Não">Não</option>
									</select>
								</label>

								<label class="w200 mleft">
									Portão Automático ou Porteiro<br>
									<select name="garagem_trabalho_tipo" id="formGaragem_trabalho_tipo" required>
										<option value=""></option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['garagem_trabalho_tipo'] == 'Sim') selected @endif value="Sim">Sim</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['garagem_trabalho_tipo'] == 'Não') selected @endif value="Não">Não</option>
									</select>
								</label>

								<br>

								<label class="w200 mbottom">
									Na Escola<br>
									<select name="garagem_escola" id="formGaragem_escola" required>
										<option value=""></option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['garagem_escola'] == 'Sim') selected @endif value="Sim">Sim</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['garagem_escola'] == 'Não') selected @endif value="Não">Não</option>
									</select>
								</label>

								<label class="w200 mbottom mleft">
									Portão Automático ou Porteiro<br>
									<select name="garagem_escola_tipo" id="formGaragem_escola_tipo" required>
										<option value=""></option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['garagem_escola_tipo'] == 'Sim') selected @endif value="Sim">Sim</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['garagem_escola_tipo'] == 'Não') selected @endif value="Não">Não</option>
									</select>
								</label>


							</div>

							<div class="secao-interna">
								<label class="w200 nowrap">
									Deseja Cobertura para Condutores de 18 a 25 anos<br>
									<select name="cobertura_condutores_risco" id="formCobertura_condutores_risco" required>
										<option value=""></option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['cobertura_condutores_risco'] == 'Sim') selected @endif value="Sim">Sim</option>
										<option @if(Session::has('formularioAuto') && Session::get('formularioAuto')['cobertura_condutores_risco'] == 'Não') selected @endif value="Não">Não</option>
									</select>
								</label>
							</div>

						</div>

						<div class="secao-form">
							<h2>OBSERVAÇÕES <span>(blindado, kit gás, etc)</span></h2>
							<div class="secao-interna">
								<textarea name="observacoes" id="formObservacoes">@if(Session::has('formularioAuto')){{Session::get('formularioAuto')['observacoes']}}@endif</textarea>
							</div>
						</div>

						<div class="submit">
							<input type="submit" value="SOLICITAR COTAÇÃO &raquo;">
						</div>

					</form>

				@endif
			</div>

		</div>
	</div>

@stop