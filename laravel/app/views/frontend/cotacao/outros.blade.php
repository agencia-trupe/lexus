@section('conteudo')

	<h1 class="pagetitle">
		<div class="centro">
			SOLICITE UMA COTAÇÃO
		</div>
	</h1>

	<div class="centro">
		<div class="internal-padding">

			<ul id="cotacoes-nav">
				<li><a href="cotacao/auto" title="SEGURO DE AUTOMÓVEIS" @if(str_is('cotacao.auto', Route::currentRouteName())) class='ativo' @endif>SEGURO DE AUTOMÓVEIS</a></li>
				<li><a href="cotacao/outros" title="DEMAIS SEGUROS" @if(str_is('cotacao.outros', Route::currentRouteName())) class='ativo' @endif>DEMAIS SEGUROS</a></li>
			</ul>

			<div class="form-outros">
				@if(Session::has('envioCotacao'))
					
					<h1 id="pagenotfound" style='margin:10px 0; line-height:140%;'>Solicitação de Cotação enviada com sucesso!<br>Retornaremos assim que possível.</h1>

				@else

					<form action="" method="post">
						<div class="form-input-largo">
							<label for="cotOutrasNome">Nome <span>(obrigatório)</span></label>
							<input type="text" name="nome" required id="cotOutrasNome">
						</div>
						<div class="form-input-menor">
							<label for="cotOutrasTelefone">Telefone</label>
							<input type="text" name="telefone" id="cotOutrasTelefone">
						</div>
						<div class="form-input-largo">
							<label for="cotOutrasEmail">E-mail <span>(obrigatório)</span></label>
							<input type="email" name="email" required id="cotOutrasEmail">
						</div>
						<div class="form-input-menor">
							<label for="cotOutrasTipo">Tipo de Seguro</label>
							<select name="tipo_seguro" id="cotOutrasTipo">
								<option value="">Selecione</option>
								@if(sizeof($listaSegurosPessoal))
									<optgroup label="Seguros Para Você">
										@foreach($listaSegurosPessoal as $seguro)
											<option value="{{ucfirst(mb_strtolower($seguro->titulo))}}">{{ucfirst(mb_strtolower($seguro->titulo))}}</option>
										@endforeach
									</optgroup>
								@endif
								@if(sizeof($listaSegurosEmpresarial))
									<optgroup label="Seguros Para Empresas">
										@foreach($listaSegurosEmpresarial as $seguro)
											<option value="{{ucfirst(mb_strtolower($seguro->titulo))}}">{{ucfirst(mb_strtolower($seguro->titulo))}}</option>
										@endforeach
									</optgroup>
								@endif								
							</select>
						</div>
						<div class="form-input-full">
							<label for="cotOutrasMsg">Mensagem</label>
							<textarea name="mensagem" id="cotOutrasMsg"></textarea>
						</div>
						<div class="form-input-submit">
							<input type="submit" value="SOLICITAR COTAÇÃO &raquo;">
						</div>
					</form>

				@endif
			</div>

		</div>
	</div>

@stop