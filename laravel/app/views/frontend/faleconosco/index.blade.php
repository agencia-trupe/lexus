@section('conteudo')

	<h1 class="pagetitle">
		<div class="centro">
			FALE CONOSCO
		</div>
	</h1>

	<div class="centro">
		<div class="internal-padding">	
			
			@if($contato->google_maps)
				{{Tools::viewGMaps($contato->google_maps, '100%', '255px')}}
			@endif
			
			<div class="colunas">
				
				<div class="texto">
					<div class="telefone">
						<span class="ddd">{{$contato->ddd}}</span>
						<span class="telefone">{{$contato->telefone}}</span>						
					</div>
					<div class="endereco">
						{{$contato->endereco}}
					</div>
					<div class="email">
						<a href="{{$contato->email}}" title="Entre em contato via e-mail">{{$contato->email}}</a>
					</div>
					<div class="social">
						@if($contato->facebook)
							<a href="{{$contato->facebook}}" title="Facebook" target="_blank" class="link-fb">Facebook</a>
						@endif
						@if($contato->twitter)
							<a href="{{$contato->twitter}}" title="Twitter" target="_blank" class="link-tw">Twitter</a>
						@endif
						@if($contato->linkedin)
							<a href="{{$contato->linkedin}}" title="LinkedIn" target="_blank" class="link-li">LinkedIn</a>
						@endif
					</div>
				</div>

				<div class="form">
					@if(Session::has('envioContato'))
						
						<h1 id="pagenotfound" style='margin:10px 0; line-height:140%;'>Obrigado por entrar em contato!<br>Retornaremos assim que possível.</h1>

					@else

						<div class="frase">{{$contato->frase}}</div>

						<form action="fale-conosco" method="post" id="form-contato" novalidate>
							<div class="coluna-1">
								<input type="text" name="nome" placeholder="Nome" required id="contato-nome">
								<input type="email" name="email" placeholder="E-mail" required id="contato-email">
								<input type="text" name="telefone" placeholder="Telefone">
							</div>
							<div class="coluna-2">
								<textarea name="mensagem" placeholder="Mensagem" required id="contato-mensagem"></textarea>
							</div>
							<div class="submit">
								<input type="submit" value="ENVIAR &raquo;">
							</div>
						</form>

					@endif
				</div>

			</div>
			
		</div>
	</div>

@stop