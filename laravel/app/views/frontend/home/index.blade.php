@section('conteudo')

	<div class="centro">

		<div class="internal-padding">

			<div id="chamadaLateral">
				@if($chamadaLateral)				
					<h1>{{$chamadaLateral->titulo}}</h1>
					<p>{{$chamadaLateral->texto}}</p>
					<a href="{{$chamadaLateral->link}}" title="{{$chamadaLateral->titulo}}" class="botao">{{$chamadaLateral->texto_botao}} &raquo;</a>				
				@endif
			</div>

			<div id="banners">
				@if(sizeof($banners))
					<div class="cycle">					
						@foreach($banners as $banner)
							<a href="{{$banner->link}}" title="{{$banner->texto}}" class="banner">
								<img src="assets/images/banners/{{$banner->imagem}}" alt="{{$banner->texto}}">
							</a>
						@endforeach					
					</div>
					<div id="banners-nav"></div>
				@endif
			</div>

			<div id="chamadas">
				@if(sizeof($chamadas))
					@foreach($chamadas as $k => $chamada)
						<a href="{{$chamada->link}}" class="chamada-{{$k}}" title="{{$chamada->titulo}}">
							<img src="assets/images/layout/home_chamada{{$k+1}}.jpg" alt="{{$chamada->titulo}}">
							<div class="icon icon-{{$k}}"></div>
							<h3>{{$chamada->titulo}}</h3>
							<p>{{$chamada->texto}}</p>
						</a>
					@endforeach
				@endif
			</div>
		
		</div>
	</div>


@stop