@section('conteudo')

	<h1 class="pagetitle">
		<div class="centro">
			LINKS ÚTEIS
		</div>
	</h1>

	<div class="centro">
		<div class="internal-padding">	

			@if(sizeof($linksUteis))
				<div class="listaLinks">
					@foreach($linksUteis as $link)
						<a href="{{$link->link}}" title="{{$link->titulo}}" class="link">
							<h2>{{$link->titulo}} <span class="icon"></span></h2>
							<span class="url">{{$link->link}}</span><span class="obs">&nbsp;-&nbsp;{{$link->texto_link}}</span>
						</a>
					@endforeach
				</div>
			@endif

		</div>
	</div>

@stop