@section('conteudo')

	<h1 class="pagetitle">
		<div class="centro">
			SEGURADORAS E PARCEIROS
		</div>
	</h1>

	<div class="centro">
		<div class="internal-padding">
			@if(sizeof($listaParceiros))
				<div class="listaParceiros">
					@foreach($listaParceiros as $parceiro)
						<div class="parceiro" title="{{$parceiro->titulo}}">
							<img src="assets/images/parceiros/{{$parceiro->imagem}}" alt="{{$parceiro->titulo}}">
						</div>
					@endforeach					
				</div>
			@endif
		</div>
	</div>

@stop