@section('conteudo')

	<h1 class="pagetitle">
		<div class="centro">
			LEXUS CORRETORA
		</div>
	</h1>

	<div class="centro">
		<div class="internal-padding">
		
			<section>
				
				<h2>QUEM SOMOS</h2>
				<div class="cke">{{$texto->texto_quemsomos}}</div>

				<h2>MISSÃO</h2>
				<div class="cke">{{$texto->texto_missao}}</div>

				<h2>VISÃO</h2>
				<div class="cke">{{$texto->texto_visao}}</div>

			</section>

			<aside>
				<img src="assets/images/layout/lexuscorretora_foto.jpg" alt="Lexus Corretora">
				
				<div class="botoes">
					<a href="seguros/para-voce" title="Seguros para você" class="link-pessoal">SEGUROS PARA<br>VOCÊ &raquo;</a>
					<a href="seguros/para-empresas" title="Seguros para empresa" class="link-empresa">SEGUROS PARA<br>EMPRESA &raquo;</a>
				</div>
			</aside>

		</div>
	</div>

@stop