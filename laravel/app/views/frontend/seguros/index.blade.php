@section('conteudo')

	<h1 class="pagetitle">
		<div class="centro">
			{{$pageTitle}}
		</div>
	</h1>

	<div class="centro">
		<div class="internal-padding">
			<aside>
				@if(sizeof($listaSeguros))
					<ul>
						@foreach($listaSeguros as $seguro)
							<li><a href="seguros/{{$slugArea}}/{{$seguro->slug}}" @if($seguro->id == $detalhe->id) class='ativo' @endif title="{{$seguro->titulo}}">{{$seguro->titulo}}</a></li>
						@endforeach
					</ul>
				@endif
			</aside>
			<section>
				@if(sizeof($detalhe))
					<div class="head">
						<img src="assets/images/{{$diretorioImagens}}/{{$detalhe->imagem}}" alt="{{$detalhe->titulo}}">
						<h1>{{$detalhe->titulo}}</h1>
					</div>
					<div class="cke">
						{{$detalhe->texto}}
					</div>
					<a href="cotacao/auto" title="CLIQUE AQUI E SOLICITE UMA COTAÇÃO" class="link-cotacao">CLIQUE AQUI E SOLICITE UMA COTAÇÃO &raquo;</a>
				@endif
			</section>			
		</div>
	</div>

@stop