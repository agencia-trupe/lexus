@section('conteudo')

	<h1 class="pagetitle">
		<div class="centro">
			EM CASO DE SINISTRO AUTO
		</div>
	</h1>

	<div class="centro">
		<div class="internal-padding">
			
			<div class="textos">
				
				<h2>{{$textos->titulo}}</h2>
				<div class="coluna"><div class="pad">{{$textos->texto_esquerda}}</div></div>
				<div class="coluna">{{$textos->texto_direita}}</div>

			</div>

			<div class="lista">
				
				<div class="sinistro-aviso">{{$textos->aviso}}</div>

				@if($num_contatos > 0)
					<div class="coluna col1">
						<?php $contador = 0 ?>
						<?php $ncol = 1 ?>
						<?php $porColuna = ceil($num_contatos/3) ?>
						@foreach($contatos as $contato)
							
							@if($contador == $porColuna)
								<?php $ncol++ ?>
								</div><div class="coluna col{{$ncol}}">
								<?php $contador = 0 ?>
							@endif
							
							<div class="seguradora">
								<div class="imagem">
									<img src="assets/images/sinistro/{{$contato->imagem}}" alt="{{$contato->titulo}}">
								</div>
								<div class="cke">{{$contato->texto}}</div>
							</div>

							<?php $contador++ ?>

						@endforeach
					</div>
				@endif
			</div>

		</div>
	</div>

@stop