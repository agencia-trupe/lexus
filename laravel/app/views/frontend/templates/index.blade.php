<!DOCTYPE html>
<html lang="pt-br" class="no-js">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="robots" content="index, follow" />
    <meta name="author" content="Trupe Design" />
    <meta name="copyright" content="2013 Trupe Design" />
    <meta name="viewport" content="width=980,initial-scale=1">
    
    <meta name="keywords" content="" />

	<title>Lexus Corretora de Seguros</title>
	<meta property="og:title" content="Lexus Corretora de Seguros"/>
	<meta name="description" content="">
	<meta property="og:description" content=""/>

    <meta property="og:site_name" content="Lexus"/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content="http://www.lexuscorretora.com.br/assets/images/facebook-share.jpg"/>
    <meta property="og:url" content="{{ Request::url() }}"/>

	<base href="{{ url() }}/">
	<script>var BASE = "{{ url() }}"</script>
	
	<?=Assets::CSS(array(
		'vendor/reset-css/reset',
		'css/fontface/stylesheet',
		'vendor/fancybox/source/jquery.fancybox',
		'css/base',
		'css/'.str_replace('-', '', Route::currentRouteName())
	))?>

	@if(isset($templateCSS))
		<?=Assets::CSS($templateCSS)?>
	@endif
	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	<script>window.jQuery || document.write('<script src="{{asset('vendor/jquery/dist/jquery.min.js')}}"><\/script>')</script>	
	

	@if(App::environment()=='local')

		<script>
			less = {
		    	env: "development",
		    	poll: 1000,
		  	};
		</script>

		<?=Assets::JS(array('vendor/modernizr/modernizr.custom-build', 'vendor/less.js/dist/less-1.6.2.min'))?>
		
		<script>less.watch();</script>

	@else
		<?=Assets::JS(array('vendor/modernizr/modernizr.custom-build'))?>
	@endif


	<script>
		// (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		// (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new
		// Date();a=s.createElement(o),
		// m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		// })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		// ga('create', 'UA-', '');
		// ga('send', 'pageview');
	</script>
</head>
<body>

	<header>
		<div id="superior">
			<div class="centro">
				
				<a href="home" title="Página Inicial" id="logo-link">
					<img src="assets/images/layout/logo.png" alt="Lexus Corretora de Seguros">
				</a>

				<nav>
					<ul>
						<li id="link-home">
							<a href="home" title="Página Inicial" @if(str_is('home', Route::currentRouteName())) class='ativo' @endif>Página Inicial</a>
						</li>
						<li>
							<a href="quem-somos" title="Quem Somos" @if(str_is('quemsomos', Route::currentRouteName())) class='ativo' @endif>LEXUS CORRETORA</a>
						</li>
						<li>
							<a href="links-uteis" title="Links Úteis" @if(str_is('linksuteis', Route::currentRouteName())) class='ativo' @endif>LINKS ÚTEIS</a>
						</li>
						<li>
							<a href="fale-conosco" title="Fale Conosco" @if(str_is('faleconosco', Route::currentRouteName())) class='ativo' @endif>FALE CONOSCO</a>
						</li>
					</ul>
				</nav>

				<div id="contato-topo">
					@if($infoContato->telefone)
						<div class="telefone">
							<span class="ddd">{{$infoContato->ddd}}</span>
							<span class="tel">{{$infoContato->telefone}}</span>
						</div>
					@endif
					@if($infoContato->email)
						<a href="mailto:{{$infoContato->email}}" title="Entre em contato">{{$infoContato->email}}</a>
					@endif
				</div>

			</div>
		</div>
		<div id="inferior">
			<div class="centro">
				<ul>
					<li>
						<a href="seguros/para-voce" title="SEGUROS PARA VOCÊ" @if(str_is('seguros.pessoais*', Route::currentRouteName())) class='ativo' @endif>
							SEGUROS<br>PARA VOCÊ
						</a>
					</li>
					<li>
						<a href="seguros/para-empresas" title="SEGUROS PARA EMPRESAS" @if(str_is('seguros.empresas*', Route::currentRouteName())) class='ativo' @endif>
							SEGUROS PARA<br>EMPRESAS
						</a>
					</li>
					<li>
						<a href="cotacao" title="SOLICITE UMA COTAÇÃO" @if(str_is('cotacao*', Route::currentRouteName())) class='ativo' @endif>
							SOLICITE<br>UMA COTAÇÃO
						</a>
					</li>
					<li>
						<a href="parceiros" title="SEGURADORAS E PARCEIROS" @if(str_is('parceiros', Route::currentRouteName())) class='ativo' @endif>
							SEGURADORAS<br>E PARCEIROS
						</a>
					</li>
					<li>
						<a href="em-caso-de-sinistro" title="EM CASO DE SINISTRO AUTO" @if(str_is('sinistro*', Route::currentRouteName())) class='ativo' @endif>
							EM CASO DE<br>SINISTRO AUTO
						</a>
					</li>
					<li id="links-social">
						@if($infoContato->facebook)
							<a href="{{$infoContato->facebook}}" title="Facebook" target="_blank" class="link-fb">Facebook</a>
						@endif
						@if($infoContato->twitter)
							<a href="{{$infoContato->twitter}}" title="Twitter" target="_blank" class="link-tw">Twitter</a>
						@endif
						@if($infoContato->linkedin)
							<a href="{{$infoContato->linkedin}}" title="LinkedIn" target="_blank" class="link-li">LinkedIn</a>
						@endif
					</li>
				</ul>
			</div>
		</div>
	</header>

	<!-- Conteúdo Principal -->
	<div class="main {{ 'main-'. str_replace('-', '', Route::currentRouteName()) }}">
		@yield('conteudo')
	</div>

	<footer>
		<div id="cadastro-newsletter">
			<div class="centro">
				<h2>CADASTRE-SE PARA RECEBER NOSSAS NOVIDADES E INFORMATIVOS:</h2>
				<form action="" method="post">
					<input type="text" name="newsletter-nome" placeholder="Nome" required>
					<input type="email" name="newsletter-email" placeholder="E-mail" required>
					<input type="submit" value="Enviar">
				</form>
			</div>
		</div>
		<div id="menu-footer">
			<div class="centro">
				<ul>
					<li><a href="home" title="Página Inicial">HOME</a></li>
					<li><a href="quem-somos" title="LEXUS CONSULTORIA">LEXUS CONSULTORIA</a></li>
					<li><a href="seguros/para-voce" title="SEGUROS PARA VOCÊ">SEGUROS PARA VOCÊ</a></li>
					@if($listaSegurosPessoais)
						@foreach($listaSegurosPessoais as $li)
							<li class='sub'><a href="seguros/para-voce/{{$li->slug}}" title="{{$li->titulo}}">{{ucfirst(mb_strtolower($li->titulo))}}</a></li>
						@endforeach
					@endif
				</ul>

				<ul>
					<li><a href="seguros/para-empresas" title="SEGUROS PARA SUA EMPRESA">SEGUROS PARA SUA EMPRESA</a></li>
					@if($listaSegurosEmpresas)
						@foreach($listaSegurosEmpresas as $li)
							<li class='sub'><a href="seguros/para-empresas/{{$li->slug}}" title="{{$li->titulo}}">{{ucfirst(mb_strtolower($li->titulo))}}</a></li>
						@endforeach
					@endif
					<li><a href="parceiros" title="SEGURADORAS E PARCEIROS">SEGURADORAS E PARCEIROS</a></li>
					<li><a href="cotacao/auto" title="SOLICITE UMA COTAÇÃO">SOLICITE UMA COTAÇÃO</a></li>
					<li><a href="em-caso-de-sinistro" title="EM CASO DE SINISTRO AUTO">EM CASO DE SINISTRO AUTO</a></li>
					<li><a href="links-uteis" title="LINKS ÚTEIS">LINKS ÚTEIS</a></li>
					<li><a href="fale-conosco" title="FALE CONOSCO">FALE CONOSCO</a></li>
				</ul>

				<div id="contato-footer">
					<img src="assets/images/layout/footer_logo.png" alt="Lexus Corretora de Seguros">
					@if($infoContato->telefone)
						<div class="telefone">
							<span class="ddd">{{$infoContato->ddd}}</span>
							<span class="tel">{{$infoContato->telefone}}</span>
						</div>
					@endif
					@if($infoContato->endereco)
						<div class="endereco">
							{{$infoContato->endereco}}							
						</div>
					@endif
					@if($infoContato->email)
						<a class="mail-link" href="mailto:{{$infoContato->email}}" title="Entre em contato">{{$infoContato->email}}</a>
					@endif
					<div id="links-social-footer">
						@if($infoContato->facebook)
							<a href="{{$infoContato->facebook}}" title="Facebook" target="_blank" class="link-fb">Facebook</a>
						@endif
						@if($infoContato->twitter)
							<a href="{{$infoContato->twitter}}" title="Twitter" target="_blank" class="link-tw">Twitter</a>
						@endif
						@if($infoContato->linkedin)
							<a href="{{$infoContato->linkedin}}" title="LinkedIn" target="_blank" class="link-li">LinkedIn</a>
						@endif
					</div>
				</div>
			</div>
		</div>
		<div id="assinatura">
			<div class="centro">
				<span>&copy; {{date('Y')}} Lexus Corretora de Seguros &bull; Todos os direitos reservados</span>
				<a href="http://www.trupe.net" title="Criação de Sites: Trupe Agência Criativa" target="_blank">Criação de Sites: Trupe Agência Criativa <img src="assets/images/layout/trupe.png" alt="Trupe Agência Criativa "></a>
			</div>
		</div>
	</footer>

	@if(isset($templateJS))
		<?=Assets::JS($templateJS)?>
	@endif

	<?=Assets::JS(array(
		'vendor/fancybox/source/jquery.fancybox',
		'vendor/jquery.cycle/jquery.cycle.all',
		'js/maskedinput',
		'js/main'
	))?>

</body>
</html>
