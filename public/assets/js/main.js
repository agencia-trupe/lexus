$('document').ready( function(){

	if($('#banners .cycle a').length > 1){
		$('#banners .cycle').cycle({
			pager:  '#banners-nav', 
		    pagerAnchorBuilder: function(idx, slide) { 
		        return '<a href="#"></a>';
		    } 
		});
	}else{
		$('#banners-nav').hide();
	}

	$('#cadastro-newsletter form').submit( function(e){
		e.preventDefault();
		var nome = $("#cadastro-newsletter form input[name=newsletter-nome]").val();
		var email = $("#cadastro-newsletter form input[name=newsletter-email]").val();

		if(nome == '' || nome == $("#cadastro-newsletter form input[name=newsletter-nome]").attr('placeholder')){
			alert('Informe seu nome!');
			return false;
		}

		if(email == '' || email == $("#cadastro-newsletter form input[name=newsletter-email]").attr('placeholder')){
			alert('Informe seu email!');
			return false;
		}
		
		$.post("cadastroNewsletter", {nome : nome, email : email}, function(retorno){
			retorno = JSON.parse(retorno);
			if(retorno.erro == '0'){
				alert('Email cadastrado com sucesso!');
				$("#cadastro-newsletter form input[name=newsletter-nome]").val('');
				$("#cadastro-newsletter form input[name=newsletter-email]").val('');
			}else{
				alert(retorno.erro)
			}
		});
	});

	$('#form-contato').submit( function(e){
		if($('#contato-nome').val() == '' || $('#contato-nome').val() == $('#contato-nome').attr('placeholder')){
			alert('Informe seu Nome!');
			e.preventDefault();
			return false;
		}
		if($('#contato-email').val() == '' || $('#contato-email').val() == $('#contato-email').attr('placeholder')){
			alert('Informe seu E-mail!');
			e.preventDefault();
			return false;
		}
		if($('#contato-mensagem').val() == '' || $('#contato-mensagem').val() == $('#contato-mensagem').attr('placeholder')){
			alert('Informe sua Mensagem!');
			e.preventDefault();
			return false;
		}
	});

	$('.form-outros form').submit( function(e){
		if($('#cotOutrasNome').val() == ''){
			alert('Informe seu Nome!');
			e.preventDefault();
			return false;
		}
		if($('#cotOutrasEmail').val() == ''){
			alert('Informe seu E-mail!');
			e.preventDefault();
			return false;
		}
	});

	$("#formTelefone").mask("(99) 9999-9999?9");
	$('#formRenovacao_vigencia').mask('9999');
	$('#formSegurado_residencia_cep').mask('99999-999');
	$('#formSegurado_pernoite_cep').mask('99999-999');
	$('#formVeiculo_ano_fabricacao').mask('9999');
	$('#formVeiculo_ano_modelo').mask('9999');
	$('#formCondutor_cpf').mask('999.999.999-99');
	$('#formSegurado_data_nascimento').mask('99/99/9999');
	$('#formCondutor_data_nascimento').mask('99/99/9999');
	$('#formVeiculo_placa').mask('aaa-9999');

	$('#formSegurado_cpfcnpj').keyup( function(){
		setTimeout( function(){			
			//Remove tudo o que não é dígito
		    var v = $('#formSegurado_cpfcnpj').val().replace(/\D/g,"")
		 
		    if (v.length <= 14) { //CPF
		 
		        //Coloca um ponto entre o terceiro e o quarto dígitos
		        v=v.replace(/(\d{3})(\d)/,"$1.$2")
		 
		        //Coloca um ponto entre o terceiro e o quarto dígitos
		        //de novo (para o segundo bloco de números)
		        v=v.replace(/(\d{3})(\d)/,"$1.$2")
		 
		        //Coloca um hífen entre o terceiro e o quarto dígitos
		        v=v.replace(/(\d{3})(\d{1,2})$/,"$1-$2")
		 
		    } else { //CNPJ
		 
		        //Coloca ponto entre o segundo e o terceiro dígitos
		        v=v.replace(/^(\d{2})(\d)/,"$1.$2")
		 
		        //Coloca ponto entre o quinto e o sexto dígitos
		        v=v.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3")
		 
		        //Coloca uma barra entre o oitavo e o nono dígitos
		        v=v.replace(/^(\d{2})\.(\d{3})\.(\d{3})(\d)/,"$1.$2.$2\/$4")
		 
		        //Coloca um hífen depois do bloco de quatro dígitos
		        v=v.replace(/\.(\d{4})(\d)/,"$1-$2")
		 
		    }
		 
		    $('#formSegurado_cpfcnpj').val(v);
		}, 1);
	});


	$('#formCotacaoAuto').submit( function(e){
		if($('#formNome').val() == ''){
			alert('Informe seu nome!');
			e.preventDefault();
			return false;
		}
		if($('#formTelefone').val() == ''){
			alert('Informe seu telefone!');
			e.preventDefault();
			return false;
		}
		if($('#formEmail').val() == ''){
			alert('Informe seu E-mail!');
			e.preventDefault();
			return false;
		}
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		if(!re.test($('#formEmail').val())){
			alert('Informe um E-mail válido!');
			e.preventDefault();
			return false;
		}
		if($('#formSegurado_nome').val() == ''){
			alert('Informe o nome do Segurado!');
			e.preventDefault();
			return false;
		}
		if($('#formSegurado_cpfcnpj').val() == ''){
			alert('Informe o CPF ou CNPJ do Segurado!');
			e.preventDefault();
			return false;
		}
		if($('#formSegurado_data_nascimento').val() == ''){
			alert('Informe a data de nascimento do Segurado!');
			e.preventDefault();
			return false;
		}
		var reg = /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g;
		if(!reg.test($('#formSegurado_data_nascimento').val())){
			alert('Informe uma Data de Nascimento do Segurado válida!');
			e.preventDefault();
			return false;
		}
		if($('#formSegurado_estado_civil').val() == ''){
			alert('Informe o estado civil do Segurado!');
			e.preventDefault();
			return false;
		}
		if($('#formSegurado_residencia_cep').val() == ''){
			alert('Informe o cep do Segurado!');
			e.preventDefault();
			return false;
		}
		if($('#formSegurado_residencia_endereco').val() == ''){
			alert('Informe o endereço do segurado!');
			e.preventDefault();
			return false;
		}
		if($('#formSegurado_pernoite_cep').val() == ''){
			alert('Informe o CEP do local de Pernoite!');
			e.preventDefault();
			return false;
		}
		if($('#formSegurado_pernoite_endereco').val() == ''){
			alert('Informe o Endereço do local de Pernoite!');
			e.preventDefault();
			return false;
		}
		if($('#formVeiculo_modelo').val() == ''){
			alert('Informe o modelo do veículo!');
			e.preventDefault();
			return false;
		}
		if($('#formVeiculo_ano_fabricacao').val() == ''){
			alert('Informe o ano de fabricação do veículo!');
			e.preventDefault();
			return false;
		}
		if($('#formVeiculo_ano_modelo').val() == ''){
			alert('Informe o ano modelo do veículo!');
			e.preventDefault();
			return false;
		}
		if($('#formVeiculo_combustivel').val() == ''){
			alert('Informe o combustível do veículo!');
			e.preventDefault();
			return false;
		}
		if($('#formVeiculo_placa').val() == ''){
			alert('Informe a placa do veículo!');
			e.preventDefault();
			return false;
		}
		if($('#formVeiculo_chassis').val() == ''){
			alert('Informe o chassis do veículo!');
			e.preventDefault();
			return false;
		}
		if($('#formVeiculo_proprietario').val() == ''){
			alert('Informe o nome do proprietário do veículo!');
			e.preventDefault();
			return false;
		}
		if($('#formCondutor_nome').val() == ''){
			alert('Informe o nome do condutor principal!');
			e.preventDefault();
			return false;
		}
		if($('#formCondutor_cpf').val() == ''){
			alert('Informe o cpf do condutor principal!');
			e.preventDefault();
			return false;
		}
		if($('#formCondutor_relacao').val() == ''){
			alert('Informe a relação do condutor principal!');
			e.preventDefault();
			return false;
		}
		if($('#formCondutor_data_nascimento').val() == ''){
			alert('Informe a data de nascimento do condutor principal!');
			e.preventDefault();
			return false;
		}
		var reg2 = /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/g;
		if(!reg2.test($('#formCondutor_data_nascimento').val())){
			alert('Informe uma Data de Nascimento do Condutor válida!');
			e.preventDefault();
			return false;
		}
		if($('#formCondutor_sexo').val() == ''){
			alert('Informe o sexo do condutor principal!');
			e.preventDefault();
			return false;
		}
		if($('#formCondutor_estado_civil').val() == ''){
			alert('Informe o estado civil do condutor principal!');
			e.preventDefault();
			return false;
		}
		if($('#formCondutor_num_carros').val() == ''){
			alert('Informe o número de carros da residência!');
			e.preventDefault();
			return false;
		}
		if($('#formGaragem_residencia').val() == ''){
			alert('Informe se o veículo ficará guardado em garagem na residência!');
			e.preventDefault();
			return false;
		}
		if($('#formGaragem_residencia_tipo').val() == ''){
			alert('Informe se a garagem de residência possui portão automático ou porteiro!');
			e.preventDefault();
			return false;
		}
		if($('#formGaragem_trabalho').val() == ''){
			alert('Informe se o veículo ficará guardado em garagem no trabalho!');
			e.preventDefault();
			return false;
		}
		if($('#formGaragem_trabalho_tipo').val() == ''){
			alert('Informe se a garagem do trabalho possui portão automático ou porteiro!');
			e.preventDefault();
			return false;
		}
		if($('#formGaragem_escola').val() == ''){
			alert('Informe se o veículo ficará guardado em garagem na escola/faculda!de');
			e.preventDefault();
			return false;
		}
		if($('#formGaragem_escola_tipo').val() == ''){
			alert('Informe se a garagem da escola/faculdade possui portão automático ou porteiro!');
			e.preventDefault();
			return false;
		}
		if($('#formCobertura_condutores_risco').val() == ''){
			alert('Informe se deseja cobertura para condutores de 18 a 25 anos!');
			e.preventDefault();
			return false;
		}
	});
});
